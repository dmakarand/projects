package com.aws.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.aws.lambda.request.EmployeeRequest;
import com.aws.lambda.request.ListEmployeeDTO;
import com.aws.lambda.service.EmpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class ServiceHandler implements RequestHandler<EmployeeRequest, Object> {

    @Autowired
    EmpService empService;

    Logger logger = LoggerFactory.getLogger(ServiceHandler.class);

    @Override
    public Object handleRequest(EmployeeRequest employeeRequest, Context context) {
        String[] str = new String[]{"Start"};
        if (!AWSLambdaDemoApplication.isRunning) {
            AWSLambdaDemoApplication.main(str);
        }
        System.out.println("Input body " + employeeRequest.toString());
        EmployeeRequest empRequestResponse = null;
        ListEmployeeDTO listEmployeeDTO = null;
        context.getLogger().log("Input: " + employeeRequest);
        try {
            switch (employeeRequest.getHttpMethod()) {
                case "POST":
                    logger.info("POST REQUEST CALLED!!");
                    empRequestResponse = ApplicationContextHolder.getBean(EmpService.class).saveEmployee(employeeRequest);
                    break;
                case "GET":
                    logger.info("GET REQUEST CALLED!!");
                    listEmployeeDTO = ApplicationContextHolder.getBean(EmpService.class).getAllEmployees();
                    break;
                default:
                    logger.info("DEFAULT REQUEST CALLED!!");
                    System.exit(0);
                    break;

            }
            logger.info("RETURNING EMPLOYEE LIST :"+listEmployeeDTO);
            return listEmployeeDTO;
        } catch (Exception e) {
            System.exit(0);
        }
        return employeeRequest;
    }

}