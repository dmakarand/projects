package com.aws.lambda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.aws.lambda.repo")
public class AWSLambdaDemoApplication {
    static public Boolean isRunning = false;

    public static void main(String[] args) {
        isRunning=true;
        SpringApplication.run(AWSLambdaDemoApplication.class, args);
    }


}