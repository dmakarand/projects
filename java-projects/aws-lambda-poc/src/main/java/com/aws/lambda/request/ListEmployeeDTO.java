package com.aws.lambda.request;

import java.util.List;

public class ListEmployeeDTO {
    private List<EmployeeRequest> employeeDTOList;

    public List<EmployeeRequest> getEmployeeDTOList() {
        return employeeDTOList;
    }

    public void setEmployeeDTOList(List<EmployeeRequest> employeeDTOList) {
        this.employeeDTOList = employeeDTOList;
    }
}
