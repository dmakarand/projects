package com.aws.lambda.request;

public class EmployeeRequest {

	private String httpMethod;

	private Integer id;

	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}


	@Override
	public String toString() {
		return "EmployeeRequest{" +
				"httpMethod='" + httpMethod + '\'' +
				", id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
