package com.aws.lambda.service;

import com.aws.lambda.AWSLambdaDemoApplication;
import com.aws.lambda.ApplicationContextHolder;
import com.aws.lambda.entity.Employee;
import com.aws.lambda.repo.EmployeeRepository;
import com.aws.lambda.request.EmployeeRequest;
import com.aws.lambda.request.ListEmployeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class EmpService {
    static Socket ignored = null;
    @Autowired
    private EmployeeRepository employeeRepository;
    Logger logger = LoggerFactory.getLogger(EmpService.class);

    public EmployeeRequest saveEmployee(EmployeeRequest employeeRequest) {
        logger.info("SAVING EMPLOYEE :" + employeeRequest.getName());
        Employee employeeSaved = null;
        if (employeeRequest.getId() != null && employeeRequest.getName() != null) {
            Employee employee = new Employee();
            employee.setId(employeeRequest.getId());
            employee.setName(employeeRequest.getName());
            employeeSaved = ApplicationContextHolder.getBean(EmployeeRepository.class).save(employee);
            if (employeeSaved != null) {
                logger.info("Employee Saved !!! : " + employeeSaved.getName());
                return employeeRequest;
            } else {
                return null;
            }
        }
        return employeeRequest;
    }

    public EmployeeRequest getEmployee(Integer id) {
        logger.info("FETCHING DETAILS OF EMPLOYEE ID :" + id);
        if (id != null) {
            Optional<Employee> employee = ApplicationContextHolder.getBean(EmployeeRepository.class).findById(id);
            if (employee.isPresent()) {
                EmployeeRequest employeeResponse = new EmployeeRequest();
                employeeResponse.setId(employee.get().getId());
                employeeResponse.setName(employee.get().getName());
                logger.info("FETCHED EMPLOYEE DETAILS SUCCESSFULLY FOR ID " + id);
                return employeeResponse;
            }
        }
        return null;
    }

    public ListEmployeeDTO getAllEmployees() {
        logger.info("FETCHING ALL EMPLOYEES");
        List<Employee> list = ApplicationContextHolder.getBean(EmployeeRepository.class).findAll();
        List<EmployeeRequest> employeeDTOS = new ArrayList<>();
        ListEmployeeDTO listEmployeeDTO = new ListEmployeeDTO();
        if (!list.isEmpty()) {
            for (Employee emp : list) {
                EmployeeRequest employeeResponse = new EmployeeRequest();
                employeeResponse.setId(emp.getId());
                employeeResponse.setName(emp.getName());
                employeeDTOS.add(employeeResponse);
            }
            listEmployeeDTO.setEmployeeDTOList(employeeDTOS);
        }
        logger.info("FETCHED ALL EMPLOYEES SUCCESFULLY :"+listEmployeeDTO);
        return listEmployeeDTO;
    }

    public String test() {
        if (!AWSLambdaDemoApplication.isRunning) {
            System.out.println("NOT ALREADY RUNNING");
        } else {
            System.out.println("ALREADY RUNNING");
        }
        return "String 123";
    }


}
