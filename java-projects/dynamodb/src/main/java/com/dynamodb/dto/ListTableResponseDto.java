package com.dynamodb.dto;

import java.util.List;


public class ListTableResponseDto {

    private List<String> tableNames ;

    public List<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(List<String> tableNames) {
        this.tableNames = tableNames;
    }
}
