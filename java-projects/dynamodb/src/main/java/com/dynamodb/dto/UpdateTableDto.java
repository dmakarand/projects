package com.dynamodb.dto;

import com.amazonaws.services.dynamodbv2.model.BillingMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateTableDto {

    String tableName;

    Long readCapacity;

    Long writeCapacity;

    BillingMode billingMode;
}
