package com.dynamodb.api;

import com.amazonaws.services.dynamodbv2.model.*;
import com.dynamodb.DynamoDB;
import com.dynamodb.dto.UpdateTableDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/dynamodb")
@RestController
public class DynamoDBController {

    @Autowired
    private DynamoDB dynamoDB;

    @PostMapping(value = "/createTable")
    public ResponseEntity<?> createTable() {
        dynamoDB.createTable();
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/listTables")
    public ResponseEntity<?> listTables() {
        return ResponseEntity.ok().body(dynamoDB.listTables());
    }

    @GetMapping(value = "/getItemsFromTable")
    public ResponseEntity<ScanResult> getItemsFromTable(@RequestParam("tableName") String tableName) {
        return ResponseEntity.ok().body(dynamoDB.getDataForTable(tableName));
    }

    @PostMapping(value = "/addItems")
    public ResponseEntity<?> addItem() {
        return ResponseEntity.ok().body(dynamoDB.addItem());
    }

    @PostMapping(value = "/createTableWithPartitionAndSortKey")
    public ResponseEntity<CreateTableResult> createTableWithPartitionAndSortKey() {
        return ResponseEntity.ok().body(dynamoDB.createTableWithPartitionAndSortKey());
    }

    @GetMapping(value = "/describeTable")
    public ResponseEntity<DescribeTableResult> describeTable
            (@RequestParam("tableName") String tableName) {
        return ResponseEntity.ok().body(dynamoDB.describeTable(tableName));
    }

    @PutMapping(value = "/updateTable")
    public ResponseEntity<UpdateTableResult> updateTable(@RequestBody UpdateTableDto updateTableDto) {
        return ResponseEntity.ok().body(dynamoDB.updateTable(updateTableDto));
    }

    @GetMapping(value = "/getItemFromKey")
    public ResponseEntity<GetItemResult> getItemFromKey(@RequestParam("tableName") String tableName, @RequestParam("value") String value) {
        return ResponseEntity.ok().body(dynamoDB.getItemFromKey(tableName, value));
    }

    @PostMapping(value = "/updateItem")
    public ResponseEntity<?> updateItem(){
        dynamoDB.updateItem();
        return ResponseEntity.ok().body(null);
    }
}
