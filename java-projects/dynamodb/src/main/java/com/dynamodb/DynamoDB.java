package com.dynamodb;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.dynamodb.config.DynamoDBConfig;
import com.dynamodb.dto.ListTableResponseDto;
import com.dynamodb.dto.UpdateTableDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class DynamoDB {

    @Autowired
    private DynamoDBConfig dynamoDBConfig;

    private Logger log = LoggerFactory.getLogger(DynamoDB.class);

    AmazonDynamoDB amazonDynamoDB = null;

    @PostConstruct
    public void setup() {
        amazonDynamoDB = dynamoDBConfig.getAmazonDynamoDBInstance();
    }

    public void createTable() {
        try {
            CreateTableRequest createTableRequest = new CreateTableRequest()
                    .withTableName("User1")
                    .withAttributeDefinitions(new AttributeDefinition(
                            "id", ScalarAttributeType.S))
                    .withKeySchema(new KeySchemaElement("id", KeyType.HASH))
                    .withProvisionedThroughput(new ProvisionedThroughput(1l, 1l));
            CreateTableResult createTableResult = amazonDynamoDB.createTable(createTableRequest);
            log.info("Table created with name " + "User");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void dropTable(String tableName) {
        try {
            amazonDynamoDB.deleteTable(tableName);
            log.info("Deleted table :" + tableName);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public ListTableResponseDto listTables() {
        ListTableResponseDto listTableResponseDto = new ListTableResponseDto();
        listTableResponseDto.setTableNames(amazonDynamoDB.listTables().getTableNames());

        return listTableResponseDto;
    }

    public ScanResult getDataForTable(String tableName) {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(tableName);
        return amazonDynamoDB.scan(scanRequest);
    }

    public CreateTableResult createTableWithPartitionAndSortKey() {
        try {
            CreateTableRequest createTableRequest = new CreateTableRequest()
                    .withTableName("Employee_profile")
                    .withAttributeDefinitions(new AttributeDefinition("name", ScalarAttributeType.S),
                            new AttributeDefinition("salary", ScalarAttributeType.N))
                    .withKeySchema(new KeySchemaElement("name", KeyType.HASH),
                            new KeySchemaElement("salary", KeyType.RANGE))
                    .withProvisionedThroughput(new ProvisionedThroughput(10l, 10l));

            return amazonDynamoDB.createTable(createTableRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public DescribeTableResult describeTable(String tableName) {
        try {
            return amazonDynamoDB.describeTable(tableName);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public UpdateTableResult updateTable(UpdateTableDto updateTableDto) {
        try {
            UpdateTableRequest updateTableRequest = new UpdateTableRequest()
                    .withTableName(updateTableDto.getTableName())
                    .withBillingMode(updateTableDto.getBillingMode())
                    .withProvisionedThroughput(new ProvisionedThroughput(10l, 10l));
            UpdateTableResult updateTableResult = amazonDynamoDB.updateTable(updateTableRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public PutItemResult addItem() {
        PutItemResult putItemResult = null;
        try {
            Map<String, AttributeValue> addItemMap = new HashMap<>();
            addItemMap.put("name", new AttributeValue("dipak"));
            addItemMap.put("salary", new AttributeValue().withN(Long.toString(12000)));
            addItemMap.put("age", new AttributeValue().withN(Long.toString(25)));
            PutItemRequest putItemRequest = new PutItemRequest().withTableName("Employee_profile").withItem(addItemMap);
            putItemResult = amazonDynamoDB.putItem(putItemRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return putItemResult;
    }

    public GetItemResult getItemFromKey(String tableName, String key) {
        try {
            Map<String, AttributeValue> map = new HashMap<>();
            map.put("name", new AttributeValue(key));
            map.put("salary",new AttributeValue().withN(Long.toString(12000)));
            GetItemRequest getItemRequest = new GetItemRequest()
                    .withTableName(tableName)
                    .withKey(map);

           return amazonDynamoDB.getItem(getItemRequest);
        }catch (Exception exception){
            exception.printStackTrace();
        }
        return null;
    }

    public void updateItem() {
        Map<String, AttributeValue> expressionAttributeNames = new HashMap();
        expressionAttributeNames.put("name", new AttributeValue().withS("makarand"));
        expressionAttributeNames.put("salary", new AttributeValue().withN(Long.toString(12000)));
        Map<String, AttributeValueUpdate> expressionAttributeValues = new HashMap();
        expressionAttributeValues.put("age", new AttributeValueUpdate().withValue(new AttributeValue().withN(Long.toString(26))));

        amazonDynamoDB.updateItem("Employee_profile",
                expressionAttributeNames, expressionAttributeValues);

    }

}
