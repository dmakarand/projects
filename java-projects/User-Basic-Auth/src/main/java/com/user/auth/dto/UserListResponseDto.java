package com.user.auth.dto;

import java.util.List;

public class UserListResponseDto {

	private  List<UserResponseDto> userList;

	public List<UserResponseDto> getUserList() {
		return userList;
	}

	public void setUserList(List<UserResponseDto> userList) {
		this.userList = userList;
	}
}
