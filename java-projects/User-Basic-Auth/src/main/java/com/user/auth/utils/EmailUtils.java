package com.user.auth.utils;

import com.user.auth.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailUtils {

    @Autowired private JavaMailSender emailSender;

    Logger log = LoggerFactory.getLogger(EmailUtils.class);

    public void sendInvitationEmail(String to, String subject, String text, String from) {
        try {
            log.info("Sending email to :" + to);
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(from);
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            emailSender.send(message);
            log.info("Email sent successfully to :" + to);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error in sending email to :" + to);
        }
    }
}
