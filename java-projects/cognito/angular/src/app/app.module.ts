import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ReceiveAuthTokenComponent } from './receive-auth-token/receive-auth-token.component';
import { LoginComponent } from './login/login.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CommonModule } from '@angular/common';

const authenticationRoutes: Routes = [

  {
    path: 'callback',
    component: ReceiveAuthTokenComponent
  },
  {
    path: 'signout',
    component: ReceiveAuthTokenComponent
  },
  {
    path: 'employee',
    component: EmployeeListComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
]
@NgModule({
  declarations: [
    AppComponent,
    ReceiveAuthTokenComponent,
    LoginComponent,
    EmployeeListComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(authenticationRoutes),
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
