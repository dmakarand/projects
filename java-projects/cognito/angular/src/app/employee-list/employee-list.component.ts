import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  public authPopupWindow;
  public users;
  public isLoginRequest: boolean = true;
  public isLoggedOut: boolean = false;
  public accessToken;
  public outhData;
  public loader;

  constructor(private _router: Router, private httpClient: HttpClient) { 
    window.onmessage = (event) => {
      if (event) {
        console.log('event-', event);
        console.log('event.data-', event.data);


        if (this.isLoginRequest == false) {
          if (
            this.authPopupWindow !== null &&
            this.authPopupWindow !== undefined
          ) {
            this.authPopupWindow.close();
          }
          localStorage.clear();
          sessionStorage.clear();
          _router.navigate(['login'])
        } else if (this.isLoginRequest == true) {
          this.authPopupWindow.close();
          _router.navigate(['employee'])
        }

        return false;

      }
    };
  }

  ngOnInit(): void {
    this.loader=true
    this.getEmployeeList()
  }


  public getEmployeeList() {

    let authtoken = localStorage.getItem('authtoken')
    if (authtoken.length) {
      const headers = new HttpHeaders()
        .set('Authorization', authtoken)
        .set('Access-Control-Allow-Origin', "*")
      console.log("auth token", authtoken)

      this.httpClient.get('http://localhost:8081/auth/cognito/list-employee', { 'headers': headers }).subscribe(res => {
        console.log("Calling list employees:", res);
        var employeeResponse = JSON.parse(JSON.stringify(res))
        this.users = employeeResponse.employeeDTOList
        this.loader=false
      }, err => {
        console.log("Calling list employees Error:", err);
        this.loader=false
      })
    }
  }


  public logout(){
    let authToken = localStorage.getItem('authToken')
    console.log('logout outh header',authToken)
    const headers= new HttpHeaders()
    .set('Authorization',authToken)
    this.httpClient.post('http://localhost:8081/auth/cognito/logout', null,{ 'headers': headers }).subscribe( res =>{
      console.log("Calling logout:", res);
      alert(res)
    }, err =>{
      console.log("logout Error:", err);

    })
    localStorage.clear;
    sessionStorage.clear;
    this.users=[]
    this.logoutViaCognito()
  }
  public configureLoginPopupWindow(popupWidth: number, popupHeight: number) {
    let dimensions = { left: 0, top: 0 };
    let screenLeft = window.screenLeft !== undefined ? window.screenLeft : 0;
    let screenTop = window.screenTop !== undefined ? window.screenTop : 0;
    let width = window.innerWidth
      ? window.innerWidth
      : document.documentElement.clientWidth
        ? document.documentElement.clientWidth
        : screen.width;
    let height = window.innerHeight
      ? window.innerHeight
      : document.documentElement.clientHeight
        ? document.documentElement.clientHeight
        : screen.height;
    dimensions.left = width / 2 - popupWidth / 2 + screenLeft;
    dimensions.top = height / 2 - popupHeight / 2 + screenTop;

    return dimensions;
  }
  public logoutViaCognito() {
   this.isLoginRequest=false;
   this.isLoggedOut=true;
    const authUrl =
      'https://agsftuserpool.auth.us-east-1.amazoncognito.com/logout?client_id=6hgtkel31e6feftf7go9sl5b9h&logout_uri=http://localhost:4200/signout';
    const popupWidth = 500;
    const popupheight = 500;
    const popupConfig = this.configureLoginPopupWindow(popupWidth, popupheight);
    this.authPopupWindow = window.open(
      authUrl,
      'popUpDiv',
      'resizable=yes, width=' +
        popupWidth +
        ', height=' +
        popupheight +
        ', top=' +
        popupConfig.top +
        ', left=' +
        popupConfig.left
    );
    if (
      this.authPopupWindow === null ||
      typeof this.authPopupWindow === undefined
    ) {
      alert('Login blocker');
    } else {
      this.authPopupWindow.cookie = [
        '*',
        '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;',
      ];
    }
   
  }
}
