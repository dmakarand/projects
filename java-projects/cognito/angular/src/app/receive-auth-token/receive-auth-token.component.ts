import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { isNull } from 'util';

@Component({
  selector: 'app-receive-auth-token',
  templateUrl: './receive-auth-token.component.html',
  styleUrls: ['./receive-auth-token.component.css']
})
export class ReceiveAuthTokenComponent implements OnInit {

  oauthData: string;

  constructor(private _route: ActivatedRoute) { }

  ngOnInit() {
    console.log("ReceiveAuthTokenComponent");

    console.log('this._route.snapshot', this._route.snapshot);
    console.log('this._route.snapshot.fragment', this._route.snapshot.fragment);


    if (this._route.snapshot.fragment && !isNull(this._route.snapshot.fragment)) {
      this.oauthData = this._route.snapshot.fragment;
      console.log('this.oauthData', this.oauthData);

    }
    if (this._route.snapshot.queryParams &&
      this._route.snapshot.queryParams.status &&
      !isNull(this._route.snapshot.queryParams.status)) {
      this.oauthData = JSON.stringify(this._route.snapshot.queryParams);
    }
    let opener = window.opener;
    opener.postMessage(this.oauthData, '*');
  }

}
