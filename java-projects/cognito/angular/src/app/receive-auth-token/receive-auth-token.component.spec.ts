import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveAuthTokenComponent } from './receive-auth-token.component';

describe('ReceiveAuthTokenComponent', () => {
  let component: ReceiveAuthTokenComponent;
  let fixture: ComponentFixture<ReceiveAuthTokenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceiveAuthTokenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveAuthTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
