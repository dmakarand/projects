import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public authPopupWindow;
  public users;
  public isLoginRequest: boolean = true;
  public isLoggedOut: boolean = false;
  public accessToken;
  public outhData;
  constructor(private _router: Router, private httpClient: HttpClient) {
    window.onmessage = (event) => {
      if (event) {
        console.log('event-', event);
        console.log('event.data-', event.data);


        if (this.isLoginRequest == false) {
          if (
            this.authPopupWindow !== null &&
            this.authPopupWindow !== undefined
          ) {
            this.authPopupWindow.close();
          }
          localStorage.clear();
          sessionStorage.clear();

        } else if (this.isLoginRequest == true) {
          const oauthObject = this.getOauthResponseObject(event.data);
          this.outhData = oauthObject
          console.log('oauthObject-', oauthObject);
          localStorage.setItem('authtoken', this.outhData.access_token)
          // this.getEmployeeList()
          this.authPopupWindow.close();
          _router.navigate(['employee'])
        }

        return false;

      }
    };
  }
  public getOauthResponseObject(fragment: string) {
    const allData = fragment.split('&');
    const dataObject = {};
    allData.forEach((element) => {
      const data = element.split('=');
      if (
        data[0] === 'access_token' ||
        data[0] === 'expires_in' ||
        data[0] === 'refresh_token'
      ) {
        dataObject[data[0]] = data[1];
      }
    });

    return dataObject;
  }

  public loginViaCognito() {
    this.isLoginRequest = true;

    const authUrl =
      'https://agsftuserpool.auth.us-east-1.amazoncognito.com/login?client_id=6hgtkel31e6feftf7go9sl5b9h&response_type=token&scope=aws.cognito.signin.user.admin+email+openid+profile&redirect_uri=http://localhost:4200/callback';
    const popupWidth = 500;
    const popupheight = 500;
    const popupConfig = this.configureLoginPopupWindow(popupWidth, popupheight);
    this.authPopupWindow = window.open(
      authUrl,
      'popUpDiv',
      'resizable=yes, width=' +
      popupWidth +
      ', height=' +
      popupheight +
      ', top=' +
      popupConfig.top +
      ', left=' +
      popupConfig.left
    );
    if (
      this.authPopupWindow === null ||
      typeof this.authPopupWindow === undefined
    ) {
      alert('Login blocker');
    } else {
      this.authPopupWindow.cookie = [
        '*',
        '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;',
      ];
    }
    this.isLoggedOut = false;
  }


  public logoutViaCognito() {
    this.isLoginRequest = false;
    this.isLoggedOut = true;
    const authUrl =
      'https://agsftuserpool.auth.us-east-1.amazoncognito.com/logout?client_id=6hgtkel31e6feftf7go9sl5b9h&logout_uri=http://localhost:4200/signout';
    const popupWidth = 500;
    const popupheight = 500;
    const popupConfig = this.configureLoginPopupWindow(popupWidth, popupheight);
    this.authPopupWindow = window.open(
      authUrl,
      'popUpDiv',
      'resizable=yes, width=' +
      popupWidth +
      ', height=' +
      popupheight +
      ', top=' +
      popupConfig.top +
      ', left=' +
      popupConfig.left
    );
    if (
      this.authPopupWindow === null ||
      typeof this.authPopupWindow === undefined
    ) {
      alert('Login blocker');
    } else {
      this.authPopupWindow.cookie = [
        '*',
        '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;',
      ];
    }

  }

  public configureLoginPopupWindow(popupWidth: number, popupHeight: number) {
    let dimensions = { left: 0, top: 0 };
    let screenLeft = window.screenLeft !== undefined ? window.screenLeft : 0;
    let screenTop = window.screenTop !== undefined ? window.screenTop : 0;
    let width = window.innerWidth
      ? window.innerWidth
      : document.documentElement.clientWidth
        ? document.documentElement.clientWidth
        : screen.width;
    let height = window.innerHeight
      ? window.innerHeight
      : document.documentElement.clientHeight
        ? document.documentElement.clientHeight
        : screen.height;
    dimensions.left = width / 2 - popupWidth / 2 + screenLeft;
    dimensions.top = height / 2 - popupHeight / 2 + screenTop;

    return dimensions;
  }

}
