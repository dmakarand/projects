import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public authPopupWindow;
  public users;
  public  isLoginRequest:boolean=true;
  public  isLoggedOut:boolean=false;
  public accessToken;
  public outhData;
  constructor(private _router: Router, private httpClient: HttpClient) {
  }

 
 

  
  public getEmployeeList(){
    const headers= new HttpHeaders()
    .set('Authorization', this.outhData.access_token)
    .set('Access-Control-Allow-Origin',"*")
    console.log("auth token",this.outhData.access_token)
    this.httpClient.get('http://localhost:8081/auth/cognito/list-employee',{ 'headers': headers }).subscribe( res =>{
      console.log("Calling list employees:", res);
      var employeeResponse= JSON.parse(JSON.stringify(res))
      this.users=employeeResponse.employeeDTOList
    }, err =>{
      console.log("Calling list employees Error:", err);

    })
  }

}
