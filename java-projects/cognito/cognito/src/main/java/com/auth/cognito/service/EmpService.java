package com.auth.cognito.service;


import com.auth.cognito.entity.Employee;
import com.auth.cognito.repo.EmployeeRepository;
import com.auth.cognito.request.EmployeeDTO;
import com.auth.cognito.request.ListEmployeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmpService {
    static Socket ignored = null;
    @Autowired
    private EmployeeRepository employeeRepository;
    Logger logger = LoggerFactory.getLogger(EmpService.class);

    RestTemplate restTemplate = new RestTemplate();

    public EmployeeDTO saveEmployee(EmployeeDTO employeeDTO) {
        
        logger.info("SAVING EMPLOYEE :" + employeeDTO.getName());
        Employee employeeSaved = null;
        if (employeeDTO.getId() != null && employeeDTO.getName() != null) {
            Employee employee = new Employee();
            employee.setId(employeeDTO.getId());
            employee.setName(employeeDTO.getName());
            employeeSaved = employeeRepository.save(employee);

            if (employeeSaved != null) {
                logger.info("Employee Saved !!! : " + employeeSaved.getName());
                return employeeDTO;
            } else {
                return null;
            }
        }
        return employeeDTO;
    }

    public ListEmployeeDTO getEmployeesFromAwsApiGateway(HttpServletRequest httpServletRequest) {
        ListEmployeeDTO listEmployeeDTO = null;
        String url = "https://uz4gxmilu8.execute-api.us-east-1.amazonaws.com/Test/employee";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", httpServletRequest.getHeader("Authorization"));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        listEmployeeDTO = restTemplate.exchange(url, HttpMethod.GET, entity, ListEmployeeDTO.class).getBody();

        return listEmployeeDTO;
    }

    public ListEmployeeDTO getEmployeeList() {
        logger.info("FETCHING DETAILS OF EMPLOYEE:");
        List<Employee> employee = employeeRepository.findAll();
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        ListEmployeeDTO listEmployeeDTO = new ListEmployeeDTO();
        if (!employee.isEmpty()) {
            for (Employee emp : employee) {
                EmployeeDTO employeeResponse = new EmployeeDTO();
                employeeResponse.setId(emp.getId());
                employeeResponse.setName(emp.getName());
                employeeDTOS.add(employeeResponse);
            }
            listEmployeeDTO.setEmployeeDTOList(employeeDTOS);
            logger.info("FETCHED EMPLOYEE DETAILS SUCCESSFULLY ");
        }
        return listEmployeeDTO;
    }
}
