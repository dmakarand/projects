package com.auth.cognito.request;

import java.util.List;

public class ListEmployeeDTO {
    private List<EmployeeDTO> employeeDTOList;

    public List<EmployeeDTO> getEmployeeDTOList() {
        return employeeDTOList;
    }

    public void setEmployeeDTOList(List<EmployeeDTO> employeeDTOList) {
        this.employeeDTOList = employeeDTOList;
    }
}
