package com.auth.cognito.service;

public class Pattern {

    public void print(int n) {
        for (int i = 1; i <= 4; i++) {
            System.out.print("*"+"\t");
            for (int j = 1; j <= 4; j++) {
                if (i == 1 || i == 4) {
                    System.out.print("*"+"\t");
                }else if(j==4){
                    System.out.print("*"+"\t");
                }else {
                    System.out.print("\t");
                }

            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Pattern pattern=new Pattern();
        pattern.print(4);
    }
}