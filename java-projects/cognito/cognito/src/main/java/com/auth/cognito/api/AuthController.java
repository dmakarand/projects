package com.auth.cognito.api;

import com.auth.cognito.dto.ResponseDto;
import com.auth.cognito.dto.ResponseObject;
import com.auth.cognito.interceptor.VerifyToken;
import com.auth.cognito.request.EmployeeDTO;
import com.auth.cognito.request.ListEmployeeDTO;
import com.auth.cognito.service.AuthService;
import com.auth.cognito.service.EmpService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping(value = "/auth/cognito")
public class AuthController {

    @Autowired
    private VerifyToken verifyToken;

    @Autowired
    private EmpService empService;

    @Autowired
    AuthService authService;

    public static final Logger LOGGER = LogManager.getLogger(AuthController.class);


    @GetMapping(value = "/list-employee")
    public ListEmployeeDTO listEmployee(HttpServletRequest httpServletRequest) {
        ListEmployeeDTO employeeList = empService.getEmployeesFromAwsApiGateway(httpServletRequest);
        return employeeList;
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<ResponseDto> logout(){
        return new ResponseEntity(authService.logout(), HttpStatus.OK);
    }

    @GetMapping(value = "/test")
    public ResponseEntity<ResponseDto> test(){
        return new ResponseEntity(new ResponseDto(new ResponseObject(200,"good",HttpStatus.OK),
                HttpStatus.OK),HttpStatus.ACCEPTED);
    }


    @PostMapping(value = "/save")
    public void save(@RequestBody EmployeeDTO employeeDTO){
        empService.saveEmployee(employeeDTO);
    }
}
