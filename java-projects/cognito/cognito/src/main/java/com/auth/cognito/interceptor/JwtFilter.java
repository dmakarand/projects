package com.auth.cognito.interceptor;

import com.auth.cognito.dto.ResponseDto;
import com.auth.cognito.dto.ResponseObject;
import com.auth.cognito.exception.UnAuthorisedException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Component
public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    private VerifyToken verifyToken;

    public JwtFilter(VerifyToken verifyToken) {
        this.verifyToken = verifyToken;
    }

    /**
     * Same contract as for {@code doFilter}, but guaranteed to be
     * just invoked once per request within a single request thread.
     * See {@link #shouldNotFilterAsyncDispatch()} for details.
     * <p>Provides HttpServletRequest and HttpServletResponse arguments instead of the
     * default ServletRequest and ServletResponse ones.
     *
     * @param request
     * @param response
     * @param filterChain
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authToken = request.getHeader("Authorization");
        try {
            if (authToken != null && !authToken.isEmpty()) {
                verifyToken.validate(request,response,authToken);
            }
            else {
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

                ResponseDto resp =
                        new ResponseDto(new ResponseObject(HttpStatus.UNAUTHORIZED.value(),
                                "UnAuthorzied Access to API", null),HttpStatus.OK);
                OutputStream outStream = response.getOutputStream();
                outStream.write(new ObjectMapper().writeValueAsString(resp).getBytes());
                outStream.flush();
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR IN FILTER TOKEN");
            e.printStackTrace();
            throw new UnAuthorisedException(401,"Un authorized access to api");
        }

    }
}
