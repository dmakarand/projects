package com.auth.cognito.exception;

import com.auth.cognito.dto.ResponseDto;
import com.auth.cognito.dto.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

	@ExceptionHandler(UnAuthorisedException.class)
	public ResponseEntity<Object> handleInvalidOldPasswordException(UnAuthorisedException invalidPasswordException) {
		ResponseDto responseDto = new ResponseDto(
				new ResponseObject(invalidPasswordException.getCode(), invalidPasswordException.getMessage(), null),
				HttpStatus.OK);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}


}
