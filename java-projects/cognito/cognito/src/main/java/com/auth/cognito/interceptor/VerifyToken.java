package com.auth.cognito.interceptor;

import com.auth.cognito.exception.UnAuthorisedException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class VerifyToken  {
    public void validate(HttpServletRequest request,HttpServletResponse httpServletResponse,String token)  {
        try {
            String aws_cognito_region = "us-east-1"; // Replace this with your aws cognito region
            String aws_user_pools_id = "us-east-1_ZN3hciKPJ"; // Replace this with your aws user pools id
            RSAKeyProvider keyProvider = new AwsCognitoRSAKeyProvider(aws_cognito_region, aws_user_pools_id);
            Algorithm algorithm = Algorithm.RSA256(keyProvider);
            JWTVerifier jwtVerifier = JWT.require(algorithm)
                    //.withAudience("2qm9sgg2kh21masuas88vjc9se") // Validate your apps audience if needed
                    .build();

            jwtVerifier.verify(token);
        }catch (Exception exception){
            System.out.println("ERROR IN VERIFY TOKEN");
            exception.printStackTrace();

            throw new UnAuthorisedException(401,"Un authorized access to api");
        }
    }
}
