package com.auth.cognito.service;

import com.auth.cognito.request.ListEmployeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthService {

    @Value("${cognito.app.client.id}")
    private String appClientId;

    @Value("${cognito.logout.callback}")
    private String callbackURL;

    @Value("${cognito.domain.name}")
    private String domainName;

    Logger log = LoggerFactory.getLogger(AuthService.class);
    public String logout() {
        try {
            String logoutURL = domainName.concat("/logout?").concat("client_id=").concat(appClientId
            ).concat("&logout_uri=").concat(callbackURL);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.exchange(logoutURL, HttpMethod.GET, null, Object.class).getBody();
            log.info("LOGGING OUT VIA COGNITO AUTH ");
            return "Logout successfully";
        } catch (Exception exception) {
            exception.printStackTrace();
            log.info("ERROR LOGGING OUT VIA COGNITO AUTH ");
            return "Logout error";
        }
    }

    public void fetchEmployeesFromGateway() {

    }
}
