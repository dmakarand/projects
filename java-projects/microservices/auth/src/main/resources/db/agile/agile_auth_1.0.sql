-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: agile_auth
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `role`
--
create database agile_auth;
use agile_auth;
DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN',NULL,NULL,NULL,NULL),(2,'USER',NULL,NULL,NULL,NULL),(3,'HR',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(45) DEFAULT NULL,
  `token` text,
  `expiry_date` datetime DEFAULT NULL,
  `token_type` varchar(255) DEFAULT NULL,
  `is_expired` bit(1) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_fk_idx` (`user_id`),
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (159,'AGSFT','2021-04-16 18:21:21','2021-04-16 18:21:21',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NTc3NDgwLCJpYXQiOjE2MTg1Nzc0ODB9.RqyIsb69X9oLIzTYosREcWBVgxxGauEZNQwJhHJorBM','2021-08-10 12:08:01','LOGIN_TOKEN',_binary '\0',1),(160,'AGSFT','2021-04-16 18:22:02','2021-04-16 18:22:02',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NTc3NTIxLCJpYXQiOjE2MTg1Nzc1MjF9.KZGr9L2ZSBQ8xCu30gs1P1VQxxIivGJSZR9TOM5O6i4','2021-08-10 12:08:42','LOGIN_TOKEN',_binary '\0',1),(161,'AGSFT','2021-04-17 18:07:24','2021-04-17 18:07:24',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NjYzMDQyLCJpYXQiOjE2MTg2NjMwNDJ9.1crmGbRuieviPM0GdKIadQLhSbhTr79NrO4vrcbF7ik','2021-08-11 11:54:03','LOGIN_TOKEN',_binary '\0',1),(162,'AGSFT','2021-04-17 18:07:32','2021-04-17 18:07:32',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NjYzMDUxLCJpYXQiOjE2MTg2NjMwNTF9.9U86pHO78PxFvcgZ-ik8irFLZNAvJvPsYNVHdz5A0zM','2021-08-11 11:54:12','LOGIN_TOKEN',_binary '\0',1),(163,'AGSFT','2021-04-17 19:05:02','2021-04-17 19:05:02',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NjY2NTAyLCJpYXQiOjE2MTg2NjY1MDJ9.v7Io8ygZyZt1lcjiyID-YqyOrYG-YK2_tliO9CwKFzY','2021-08-11 12:51:42','LOGIN_TOKEN',_binary '\0',1),(164,'AGSFT','2021-04-17 19:39:17','2021-04-17 19:39:17',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NjY4NTU3LCJpYXQiOjE2MTg2Njg1NTd9.ns5rzOyOa5pxKtv3ohprpS3VAH-AHuAm_eaYjJC0Bj8','2021-08-11 13:25:57','LOGIN_TOKEN',_binary '\0',1),(165,'AGSFT','2021-04-17 19:39:39','2021-04-17 19:39:39',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NjY4NTc5LCJpYXQiOjE2MTg2Njg1Nzl9.5OauTRXp9sQwTS2-gpVv0hBIFXujdNWDneg8kKHlUm8','2021-08-11 13:26:19','LOGIN_TOKEN',_binary '\0',1),(166,'AGSFT','2021-04-17 20:09:01','2021-04-17 20:09:01',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4NjcwMzQxLCJpYXQiOjE2MTg2NzAzNDF9.FZobNP2yLExm2sXJRzsnYzKqYtyI6bTT1m0woSRCq84','2021-08-11 13:55:41','LOGIN_TOKEN',_binary '\0',1),(167,'AGSFT','2021-04-19 11:14:44','2021-04-19 11:14:44',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI4ODExMDgzLCJpYXQiOjE2MTg4MTEwODN9.8__Ag6J4bPrP3bbbuiziajRpPCnKb-OfYLPDr7ttyxo','2021-08-13 05:01:24','LOGIN_TOKEN',_binary '\0',1),(168,'AGSFT','2021-04-23 16:11:22','2021-04-23 16:11:22',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5MTc0NDgxLCJpYXQiOjE2MTkxNzQ0ODF9.JhQ17nuWVSTWM_mMFfQknXxSEjUy7yQ_17UmxxZciIo','2021-08-17 09:58:02','LOGIN_TOKEN',_binary '\0',1),(169,'AGSFT','2021-04-23 16:16:06','2021-04-23 16:16:06',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5MTc0NzY2LCJpYXQiOjE2MTkxNzQ3NjZ9.VCM84XZMlJ_0vuC8Z5yqg0VdO5jFOWUckwtatZqw5kc','2021-08-17 10:02:46','LOGIN_TOKEN',_binary '\0',1),(170,'agile@agsft.com','2021-04-23 16:17:04','2021-04-23 16:17:04','agile@agsft.com','eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiVVNFUiIsInJvbGVzIjpbeyJyb2xlSWQiOjIsInJvbGUiOiJVU0VSIn1dLCJ1c2VyTmFtZSI6ImpzYW9yYWJoQHlvcG1haWwuY29tIiwiZXhwIjoxNjI5MTc0ODIzLCJpYXQiOjE2MTkxNzQ4MjN9.4YjuhNfJScHA3Abe2gdHaeemPp5A_olTAkYjDVyWHG0','2021-05-05 06:03:44','RESET_PASSWORD_TOKEN',NULL,2),(171,'AGSFT','2021-04-23 16:17:17','2021-04-23 16:17:17',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5MTc0ODM3LCJpYXQiOjE2MTkxNzQ4Mzd9.cvq3QhT2D0aUJp49hcgsCzJXBZ8z4R-YYyAQ0mDjKUM','2021-08-17 10:03:57','LOGIN_TOKEN',_binary '\0',1),(172,'AGSFT','2021-04-23 16:26:50','2021-04-23 16:26:50',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5MTc1NDA5LCJpYXQiOjE2MTkxNzU0MDl9.ig1RNbOJMKLWf6c0S6uOAfq0UhK0RKF83Ule-SU2H8c','2021-08-17 10:13:30','LOGIN_TOKEN',_binary '\0',1),(173,'agile@agsft.com','2021-04-23 16:27:16','2021-04-23 16:27:16','agile@agsft.com','eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiSFIiLCJyb2xlcyI6W3sicm9sZUlkIjozLCJyb2xlIjoiSFIifV0sInVzZXJOYW1lIjoiaHJhZG1pbkB5b3BtYWlsLmNvbSIsImV4cCI6MTYyOTE3NTQzNiwiaWF0IjoxNjE5MTc1NDM2fQ.4TcU4N_CW_nEdzh2506u7x9F7OTtSer6REon3qV5X3I','2021-05-05 06:13:56','RESET_PASSWORD_TOKEN',NULL,3),(174,'AGSFT','2021-04-26 10:54:21','2021-04-26 10:54:21',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5NDE0NjYxLCJpYXQiOjE2MTk0MTQ2NjF9.IQOTWKsLZUTd3TjH0cMvzl1PslqIS13ncSEZ47jvuNo','2021-08-20 04:41:01','LOGIN_TOKEN',_binary '\0',1),(175,'AGSFT','2021-04-26 10:56:22','2021-04-26 10:56:22',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5NDE0NzgxLCJpYXQiOjE2MTk0MTQ3ODF9.2rekSizn4vH33_sazC5g3M45j6OQ0UKdWTLzToyULXs','2021-08-20 04:43:02','LOGIN_TOKEN',_binary '\0',1),(176,'AGSFT','2021-04-27 09:37:55','2021-04-27 09:37:55',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5NDk2NDc0LCJpYXQiOjE2MTk0OTY0NzR9.ocZgVodxXKGRsODy4NMKvM13QnI0lPSzBhfyOvc8Udk','2021-08-21 03:24:35','LOGIN_TOKEN',_binary '\0',1),(177,'AGSFT','2021-04-27 09:38:35','2021-04-27 09:38:35',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5NDk2NTE1LCJpYXQiOjE2MTk0OTY1MTV9.6CZtehT758eFmP0ne1CLNe-oE57z-GEGwg8ueJmkUz0','2021-08-21 03:25:15','LOGIN_TOKEN',_binary '\0',1),(178,'AGSFT','2021-04-27 09:39:04','2021-04-27 09:39:04',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5NDk2NTQ0LCJpYXQiOjE2MTk0OTY1NDR9.bTVaWOjNfPJ8a5kZjzhiMefXQVSf_BEifgUddPLgILo','2021-08-21 03:25:44','LOGIN_TOKEN',_binary '\0',1),(179,'AGSFT','2021-04-27 11:04:01','2021-04-27 11:04:01',NULL,'eyJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiQURNSU4iLCJyb2xlcyI6W3sicm9sZUlkIjoxLCJyb2xlIjoiQURNSU4ifV0sInVzZXJOYW1lIjoiYWdpbGVAYWdzZnQuY29tIiwiZXhwIjoxNjI5NTAxNjQxLCJpYXQiOjE2MTk1MDE2NDF9.sPruM3M0uo0deeWu7ulXaaT72_-pJrUihvpQHaD85bI','2021-08-21 04:50:41','LOGIN_TOKEN',_binary '\0',1);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `last_modified_by` varchar(45) DEFAULT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'AGSFT','agile@agsft.com','$2a$10$07RTspl5/bIn8wzMkLtcL.yBenMcI5mzqWZLkAIlxHx4EeMyX66lm',NULL,NULL,NULL,NULL,_binary '\0'),(2,NULL,'jsaorabh@yopmail.com','$2a$10$egwvkMZHyLNNqcnX1LMFHuV5Bjzeinlvg4t7JPRYnC29ZbXSgJINu','2021-04-23 16:17:04.123000','agile@agsft.com','2021-04-23 16:17:04.123000','agile@agsft.com',_binary '\0'),(3,NULL,'hradmin@yopmail.com','$2a$10$hTXH2dMNHkFo0FTAgYLVl./tyYfkil.pyOO6s8HbSqJrwYaB9mKyu','2021-04-23 16:27:16.454000','agile@agsft.com','2021-04-23 16:27:16.454000','agile@agsft.com',_binary '\0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `role_id_idx` (`role_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (4,1,1),(5,2,2),(6,3,3);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-28 12:01:39
