package com.user.auth.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user.auth.dto.request.ChangePasswordDto;
import com.user.auth.dto.request.ForgotPasswordDto;
import com.user.auth.dto.request.ResetPasswordReqDto;
import com.user.auth.dto.request.UserLoginReqDto;
import com.user.auth.dto.response.ResponseDto;
import com.user.auth.dto.response.ResponseObject;
import com.user.auth.dto.response.UserDto;
import com.user.auth.dto.response.UserListResponseDto;
import com.user.auth.security.JwtProvider;
import com.user.auth.service.AuthService;
import com.user.auth.service.impl.UserServiceImpl;
import com.user.auth.utils.UserAuthUtils;

/**
 * This class represents an endpoint of user authentication services
 */
@RestController
@RequestMapping(value = "/api/auth")
public class AuthController {

	@Autowired
	private AuthService authService;

	@Autowired
	private UserAuthUtils userAuthUtils;

	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private JwtProvider jwtProvider;
	Logger logger = LoggerFactory.getLogger(AuthController.class);

	/**
	 * This method is used for reset forgotten password.
	 * 
	 * @param forgotDto
	 * @return send password token to registered user
	 * @author dipak
	 * @date 09/02/2021
	 */
	@PostMapping(path = "user/forgotPassword")
	public ResponseEntity forgotPassword(@RequestBody ForgotPasswordDto forgotDto, HttpServletRequest request)
			throws Exception {
		String userAgent = request.getHeader("User-Agent");
		logger.info("User agent info is {}" + userAgent);
		authService.forgotPassword(forgotDto);
		ResponseDto responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(),
				"Your password token is sent to your registered email id.", null), HttpStatus.OK);
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	/**
	 * This method is used for changing password.
	 * 
	 * @param changePasswordDto
	 * @return success message of changed password
	 * @author dipak
	 * @date 09/02/2021
	 */
	@PreAuthorize("hasAnyAuthority('ADMIN','USER')")
	@PostMapping(path = "/user/changePassword")
	public ResponseEntity changePassword(@RequestBody ChangePasswordDto changePasswordDto) {
		authService.changePassword(changePasswordDto);
		ResponseDto responseDto = new ResponseDto(
				new ResponseObject(HttpStatus.OK.value(), "Password changed successfully..!", null), HttpStatus.OK);
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	/**
	 * This method is an endpoint for logging in the user
	 * 
	 * @param dto
	 * @return user information including jwt token
	 * @author aakash
	 * @date 09/02/2021
	 */
	@PostMapping(path = "/user/login")
	public ResponseEntity loginUser(@RequestBody UserLoginReqDto dto, HttpServletRequest servletRequest) {
		UserDto response = authService.loginUser(dto, servletRequest);
		ResponseDto responseDto = new ResponseDto(
				new ResponseObject(HttpStatus.OK.value(), "Logged in successfully", response), HttpStatus.OK);
		logger.info("Request received for login");
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	/**
	 * This method is an endpoint to reset user's password
	 *
	 * @param dto containing user one time password and email address
	 * @return Success message of user activation
	 */
	@PostMapping(path = "/user/resetPassword")
	public ResponseEntity resetPassword(@RequestParam("token") String token, @RequestBody ResetPasswordReqDto dto) {
		UserDto response = authService.resetPassword(dto, token);
		ResponseDto responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(),
				"User is Activated and changed password successfully", response), HttpStatus.OK);
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	@PostMapping(path = "/user/logout")
	public ResponseEntity logout(HttpServletRequest httpServletRequest) {
		ResponseDto responseDto;
		authService.logout(httpServletRequest);
		responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(), "Logged out successfully", null),
				HttpStatus.OK);
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	@PostMapping(path = "/user/check/token/expiry")
	public Boolean isTokenExpired(HttpServletRequest httpServletRequest) {
		return jwtProvider.isTokenExpired(httpServletRequest.getHeader("Authorization"));
	}

	@GetMapping(value = "/test")
	public String hello() {
		return "makarand";
	}

	@PreAuthorize("hasAnyAuthority('ADMIN')")
	@PostMapping(value = "/addUser")
	public ResponseEntity<ResponseDto> addUser(@RequestBody UserDto userDto, HttpServletRequest httpServletRequest)
			throws IOException, DocumentException, Exception {
		ResponseDto responseDto;
		try {
			userServiceImpl.addUser(userDto, httpServletRequest);
			responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(), "User added successfully", null),
					HttpStatus.OK);
		} catch (Exception e) {
			responseDto = new ResponseDto(new ResponseObject(400, e.getMessage(), null), HttpStatus.OK);
		}

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	@PreAuthorize("hasAnyAuthority('ADMIN')")
	@GetMapping(value = "/user/getAllUsers")
	public ResponseEntity<UserListResponseDto> getAllUsers(@RequestParam("role") String role) {
		UserListResponseDto userList = userServiceImpl.getAllUsers(role);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(userList);
	}

	@GetMapping(value = "/user/getApiSecretKey")
	public ResponseEntity<ResponseDto> getApiSecretKey(HttpServletRequest httpServletRequest) {
		ResponseDto responseDto=null;
		try {
			 String response = userServiceImpl.getApiSecretKey(httpServletRequest);
			responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(), "Api Secret Key Fetched Successfully", response),
					HttpStatus.OK);
		} catch (Exception e) {
			responseDto = new ResponseDto(new ResponseObject(400, e.getMessage(), null), HttpStatus.OK);
		}
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

}
