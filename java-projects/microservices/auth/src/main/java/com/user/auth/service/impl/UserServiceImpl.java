package com.user.auth.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.dom4j.DocumentException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.user.auth.dto.response.ResponseDto;
import com.user.auth.dto.response.UserDto;
import com.user.auth.dto.response.UserListResponseDto;
import com.user.auth.enums.TokenType;
import com.user.auth.exception.InvalidEmailException;
import com.user.auth.exception.InvalidRequestException;
import com.user.auth.model.Role;
import com.user.auth.model.Token;
import com.user.auth.model.User;
import com.user.auth.repository.RoleRepository;
import com.user.auth.repository.TokenRepository;
import com.user.auth.repository.UserRepository;
import com.user.auth.security.JwtProvider;
import com.user.auth.utils.EmailUtils;

@Service
public class UserServiceImpl {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private EmailUtils emailUtils;

	@Value("${forgot.token.validity}")
	private Long forgotTokenValidity;

	@Value("${reset.otp.size}")
	private int otpSize;

	@Value("${mail.activate-user-url}")
	private String activateUserApiUrl;

	@Value("${mail.profile.activation.subject}")
	private String activationEmailSubject;

	@Value("${reset.token.validity}")
	private Long resetTokenExpiry;

	@Value("${jwt.tokenValidity}")
	private Long jwTokenExpiry;

	@Value("${spring.mail.username}")
	private String fromEmail;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private RestTemplate restTemplate;

	private Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Transactional(rollbackOn = Throwable.class)
	public void addUser(UserDto userDTO, HttpServletRequest httpServletRequest)
			throws IOException, DocumentException, InvalidRequestException, InvalidEmailException {

		if (userDTO == null || userDTO.getEmail() == null || userDTO.getRoles().isEmpty()
				|| userDTO.getRoles() == null) {
			throw new InvalidRequestException("Invalid request");
		}
		log.info("Saving user :" + userDTO.getEmail());
		Optional<User> existingUser = userRepository.findByEmail(userDTO.getEmail());
		boolean isDeletedUser = existingUser.isPresent() && existingUser.get().getDeleted();
		String profile_path;
		if (!existingUser.isPresent() || isDeletedUser) {
			User mappedUser = modelMapper.map(userDTO, User.class);
			mappedUser.setDeleted(Boolean.FALSE);
			User user = isDeletedUser ? existingUser.get() : mappedUser;
			if (!isDeletedUser) {
				List<Role> roles = new ArrayList<>();
				for (String r : userDTO.getRoles()) {
					Optional<Role> role = roleRepository.findByRole(r);
					role.ifPresent(roles::add);
				}
				mappedUser.setRoles(roles);
				Token token = new Token();
				token.setToken(jwtProvider.generateToken(user, null));
				token.setTokenType(TokenType.RESET_PASSWORD_TOKEN);
				token.setUsers(mappedUser);
				token.setExpiryDate(new Date(System.currentTimeMillis() + resetTokenExpiry * 1000));
				mappedUser.setTokens(Collections.singletonList(token));
				mappedUser.setPassword(passwordEncoder.encode(token.getToken()));
				User savedUser = userRepository.save(mappedUser);
				userDTO.setUserId(savedUser.getUserId());
				String message = "Hello \t\t" + userDTO.getFirstName()
						+ "Please activate your account by clicking this link.";
				String activationUrl = emailUtils.buildUrl(token.getToken(), activateUserApiUrl);
				emailUtils.sendInvitationEmail(mappedUser.getEmail(), activationEmailSubject, message, fromEmail,
						activationUrl);
				saveEmployee(userDTO, httpServletRequest);
			} else
				throw new InvalidEmailException(messageSource.getMessage("invalid.email", null, Locale.ENGLISH));
			log.info("User saved successfully : " + userDTO.getEmail());
		} else {
			throw new InvalidRequestException(messageSource.getMessage("user.already.exist", null, null));
		}
	}

	private void saveEmployee(UserDto userDto, HttpServletRequest httpServletRequest) throws InvalidRequestException {
		ResponseEntity<ResponseDto> response;

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", httpServletRequest.getHeader("Authorization"));
		headers.add("tenant", httpServletRequest.getHeader("tenant"));
		HttpEntity<UserDto> entity = new HttpEntity<UserDto>(userDto, headers);

		response = restTemplate.exchange("http://EMPLOYEE-SERVICE/api/employee/addEmployee", HttpMethod.POST, entity,
				ResponseDto.class);
		if (response.getBody().getResponseObject().getCode() != 200) {
			throw new InvalidRequestException("Unexpected error occured while saving employee");

		}

		log.info("Employee service response " + response);
	}

	public UserListResponseDto getAllUsers(String role) {
		UserListResponseDto userListResponse = new UserListResponseDto();
		try {

			log.info("Fetching users from auth db with role" + role);
			Optional<Role> roleFromDB = roleRepository.findByRole(role);
			if (!roleFromDB.isPresent()) {

			}
			List<User> userList = userRepository.findByRoles(roleFromDB.get());
			List<UserDto> userDTOList = userList.stream().map(x -> modelMapper.map(x, UserDto.class))
					.collect(Collectors.toList());

			userListResponse.setUserList(userDTOList);
			log.info("Fetched users from auth db with role" + role);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userListResponse;
	}

	public String getApiSecretKey(HttpServletRequest httpServletRequest) {
		String tenant = httpServletRequest.getHeader("tenant");
		String key = "";
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + "master_db", "root", "root");

			PreparedStatement ps = connection.prepareStatement("select api_secret_key from account where name=?");
			ps.setString(1, tenant);
			ResultSet rs = ps.executeQuery();
			rs.next();
			 key = rs.getString("api_secret_key");
			return key;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return tenant;

	}

}
