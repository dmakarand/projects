package com.employeeservice;

import com.employeeservice.service.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmployeeServiceApplicationTests {

    @Test
    public void print(){
        System.out.println("Employee Service test executed");
    }

    @Test
    public void add(){
        Calculator calculator = new Calculator();
        int result=calculator.add(5,5);
        int expected = 10;
        Assertions.assertEquals(10,result);
    }
}
