package com.employeeservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employeeservice.dto.EmployeeDTO;
import com.employeeservice.dto.ResponseDto;
import com.employeeservice.dto.ResponseObject;
import com.employeeservice.enums.RoleConstants;
import com.employeeservice.service.EmployeeService;

@RestController
@RequestMapping(value = "/api/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PreAuthorize("hasAnyAuthority('ADMIN')")
	@GetMapping(value = "/getAllEmployee")
	public ResponseEntity<ResponseDto> getAllemployee(@RequestParam("role") RoleConstants role,
			HttpServletRequest httpServletRequest) {
		ResponseDto responseDto;
		List<EmployeeDTO> employeeList = employeeService.getAllEmployees(role, httpServletRequest);
		responseDto = new ResponseDto(
				new ResponseObject(HttpStatus.OK.value(), "Employees fetched successfully", employeeList),
				HttpStatus.OK);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	@PreAuthorize("hasAnyAuthority('ADMIN')")
	@PostMapping(value = "/addEmployee")
	public ResponseEntity<ResponseDto> saveEmployee(@RequestBody EmployeeDTO employeeDTO,
			HttpServletRequest httpServletRequest) {
		ResponseDto responseDto;
		employeeService.saveEmployee(employeeDTO, httpServletRequest);
		responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(), "Employee Added successfully", null),
				HttpStatus.OK);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

	@GetMapping(value = "/test1")
	public String hi() {
		return "makarand";
	}
}
