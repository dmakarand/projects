package com.employeeservice.dto;

import java.util.List;

public class EmployeeListDTO {

	private List<EmployeeDTO> employeList;

	public List<EmployeeDTO> getEmployeList() {
		return employeList;
	}

	public void setEmployeList(List<EmployeeDTO> employeList) {
		this.employeList = employeList;
	}

}
