package com.employeeservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.employeeservice.dto.EmployeeDTO;
import com.employeeservice.dto.ResponseDto;
import com.employeeservice.dto.SalaryDto;
import com.employeeservice.dto.UserDto;
import com.employeeservice.dto.UserListResponseDto;
import com.employeeservice.enums.RoleConstants;
import com.employeeservice.exception.ServiceException;
import com.employeeservice.model.Employee;
import com.employeeservice.repo.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private RestTemplate restTemplate;

	private Logger log = LoggerFactory.getLogger(EmployeeService.class);

	public List<EmployeeDTO> getAllEmployees(RoleConstants role, HttpServletRequest httpServletRequest) {
		List<EmployeeDTO> employeeList = new ArrayList<EmployeeDTO>();
		log.info("EMPLOYEE SERVICE : Fetching employee list of role :" + role);
		UserListResponseDto userList = getAllUsers(httpServletRequest, role);
		for (UserDto user : userList.getUserList()) {
			Employee emp = employeeRepository.findByUserId(user.getUserId());
			EmployeeDTO empDTO = modelMapper.map(emp, EmployeeDTO.class);
			empDTO.setEmail(user.getEmail());
			employeeList.add(empDTO);
		}
		log.info("EMPLOYEE SERVICE : Employee fetched successfully of role :" + role);
		return employeeList;

	}

	@Transactional
	public void saveEmployee(EmployeeDTO employeeDTO, HttpServletRequest httpServletRequest) throws ServiceException {
		if (employeeDTO == null) {
			throw new ServiceException("Invalid request ");
		}
		log.info("Saving employee :" + employeeDTO.getEmail());
		Employee employee = modelMapper.map(employeeDTO, Employee.class);
		Employee savedEmployee = employeeRepository.save(employee);
		if (savedEmployee == null) {
			throw new ServiceException("New employee not saved");
		}
		SalaryDto salaryDto = new SalaryDto();
		salaryDto.setEmployeeId(employee.getId());
		salaryDto.setSalary(employeeDTO.getSalary());
		saveSalary(salaryDto, httpServletRequest);
		log.info("EMPLOYEE SERVICE :Employee saved successfully :" + employee.getFirstName() + ""
				+ employee.getLastName());
	}

	private UserListResponseDto getAllUsers(HttpServletRequest httpServletRequest, RoleConstants role) {
		log.info("REQUESTING USER SERVICE FOR USER LIST");
		// rest template
		UserListResponseDto userListResponseList = new UserListResponseDto();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", httpServletRequest.getHeader("Authorization"));
		headers.add("tenant", httpServletRequest.getHeader("tenant"));
		HttpEntity entity = new HttpEntity(headers);

		ResponseEntity<UserListResponseDto> response = restTemplate.exchange(
				"http://AUTH-SERVICE/api/auth/user/getAllUsers?role=" + role.name(), HttpMethod.GET, entity,
				UserListResponseDto.class);
//		List<UserDto> userDto = (List<UserDto>) userListResponse.get("userList");
		userListResponseList.setUserList(response.getBody().getUserList());
		log.info("User list response " + response);
		return userListResponseList;
	}

	private void saveSalary(SalaryDto salaryDto, HttpServletRequest httpServletRequest) throws ServiceException {
		ResponseEntity<ResponseDto> salaryResponse = null;
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", httpServletRequest.getHeader("Authorization"));
			headers.add("tenant", httpServletRequest.getHeader("tenant"));
			HttpEntity<SalaryDto> entity = new HttpEntity<SalaryDto>(salaryDto, headers);

			salaryResponse = restTemplate.exchange("http://SALARY-SERVICE/api/salary/saveSalary", HttpMethod.POST, entity,
					ResponseDto.class);
			if (salaryResponse == null) {
				throw new ServiceException("Unexpected error occurred while saving salary");
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			throw new ServiceException(exception.getMessage());
		}
		log.info("Salary service response " + salaryResponse);
	}
}
