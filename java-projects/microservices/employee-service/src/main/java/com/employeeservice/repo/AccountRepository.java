package com.employeeservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeservice.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findByName(String tenant);
}
