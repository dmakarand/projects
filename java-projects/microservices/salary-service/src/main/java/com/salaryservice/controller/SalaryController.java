package com.salaryservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.salaryservice.dto.ResponseDto;
import com.salaryservice.dto.ResponseObject;
import com.salaryservice.dto.SalaryDTO;
import com.salaryservice.service.SalaryService;

@RestController
@RequestMapping(value = "/api/salary")
public class SalaryController {

	@Autowired
	private SalaryService salaryService;

	@GetMapping(value = "/getSalary")
	public SalaryDTO getSalary(@RequestParam("employeeId") Long employeeId) {
		return salaryService.getSalary(employeeId);
	}

	@PostMapping(value = "/saveSalary")
	public ResponseEntity<ResponseDto> saveSalary(@RequestBody SalaryDTO salaryDTO) {
		ResponseDto responseDto;
		salaryService.saveSalary(salaryDTO);
		responseDto = new ResponseDto(new ResponseObject(HttpStatus.OK.value(), "Employee Salary successfully", null),
				HttpStatus.OK);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(responseDto);
	}

}
