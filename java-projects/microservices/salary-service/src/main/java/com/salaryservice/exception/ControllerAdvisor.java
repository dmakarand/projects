package com.salaryservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.salaryservice.dto.ResponseDto;
import com.salaryservice.dto.ResponseObject;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<Object> invalidRequest(ServiceException invalidRequest) {
		ResponseDto responseDto = new ResponseDto(
				new ResponseObject(invalidRequest.getCode(), invalidRequest.getMessage(), null), HttpStatus.OK);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> genericException(Exception invalidRequest) {
		ResponseDto responseDto = new ResponseDto(new ResponseObject(400, invalidRequest.getMessage(), null),
				HttpStatus.OK);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

}
