package com.salaryservice.service;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salaryservice.dto.SalaryDTO;
import com.salaryservice.exception.ServiceException;
import com.salaryservice.model.Salary;
import com.salaryservice.repository.SalaryRepository;

@Service
public class SalaryService {

	@Autowired
	private SalaryRepository salaryRepository;

	@Autowired
	private ModelMapper modelMapper;

	public SalaryDTO getSalary(Long employeeId) {

		if (employeeId == 0 || employeeId == null) {
			throw new ServiceException("Invalid Request");
		}
		Salary salary = salaryRepository.findByEmployeeId(employeeId);
		if (salary == null) {
			throw new ServiceException("Salary not found");
		}
		SalaryDTO salaryDTO = modelMapper.map(salary, SalaryDTO.class);
		return salaryDTO;
	}

	@Transactional
	public void saveSalary(SalaryDTO salaryDTO) {
		if (salaryDTO == null || salaryDTO.getEmployeeId() == null || salaryDTO.getSalary() == 0) {
			throw new ServiceException("Invalid Request");
		}
		Salary salary = modelMapper.map(salaryDTO, Salary.class);
		salaryRepository.save(salary);
	}
}
