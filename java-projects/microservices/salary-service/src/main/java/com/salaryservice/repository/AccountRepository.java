package com.salaryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.salaryservice.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findByName(String tenant);
}
