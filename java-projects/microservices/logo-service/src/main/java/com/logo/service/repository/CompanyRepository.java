package com.logo.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logo.service.model.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company,Long>{

	Company findByName(String tenant);

}
