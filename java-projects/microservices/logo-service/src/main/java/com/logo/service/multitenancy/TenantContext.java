package com.logo.service.multitenancy;

public class TenantContext {

    final public static String DEFAULT_TENANT = "master_db";

    private static ThreadLocal<String> currentTenant = new ThreadLocal<String>() {
        @Override
        protected String initialValue() {
            return DEFAULT_TENANT;
        }
    };

    public  static String getCurrentTenant() {
        return currentTenant.get();
    }

    public  static void setCurrentTenant(String tenant) {
        currentTenant.set(tenant);
    }

    public static void clear(){
        currentTenant.set(null);
    }
}
