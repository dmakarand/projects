package com.logo.service.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.logo.service.model.Account;
import com.logo.service.multitenancy.TenantContext;
import com.logo.service.repository.AccountRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class Interceptor extends OncePerRequestFilter {

	@Autowired
	private AccountRepository accountRepository;

	public Interceptor(AccountRepository accountRepository) {
		super();
		this.accountRepository = accountRepository;
	}

	private Logger log = LoggerFactory.getLogger(Interceptor.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

	
		String tenant = request.getHeader("tenant");
	
		tenant = tenant.replaceAll("\\[", "").replaceAll("\\]", "");
		TenantContext.clear();
		Account account = accountRepository.findByName(tenant);
		try {
			if (account == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		tenant = tenant.concat("_company");
		TenantContext.setCurrentTenant(tenant);
		filterChain.doFilter(request, response);

	}

}
