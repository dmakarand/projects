package com.logo.service.dto;

public class CompanyDTO {

	private String companyName;
	
	private byte[] logoImage;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public byte[] getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(byte[] logoImage) {
		this.logoImage = logoImage;
	}
	
	
}
