package com.logo.service.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logo.service.dto.CompanyDTO;
import com.logo.service.services.CompanyServices;

@RestController
@RequestMapping(value = "/api/company")

public class CompanyController {
	
	@Autowired
	private CompanyServices companyService;
	
	private Logger log = LoggerFactory.getLogger(CompanyController.class);
	
	@GetMapping(value = "/getInfo")
	public CompanyDTO getCompanyInfo (HttpServletRequest servletRequest) throws Exception {
		String tenant = servletRequest.getHeader("tenant");
		return companyService.getCompanyInfo(tenant);
	}

	
}
