package com.logo.service.services;

import java.io.File;
import java.nio.file.Paths;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import com.google.common.io.Files;
import com.logo.service.dto.CompanyDTO;
import com.logo.service.model.Company;
import com.logo.service.repository.CompanyRepository;

@Service
public class CompanyServices {

	@Autowired
	private CompanyRepository companyRepo;

	@Autowired
	private ModelMapper modelMapper;

	public CompanyDTO getCompanyInfo(String tenant) throws Exception {

		if (tenant.isBlank() || tenant.isEmpty()) {
			throw new Exception("TENANT NAME IS EMPTY");
		}
		Company company = companyRepo.findByName(tenant);
		CompanyDTO companyDTO = modelMapper.map(company, CompanyDTO.class);
		
		Resource resource = new ClassPathResource(company.getLogoPath());
		companyDTO.setCompanyName(company.getName());
		companyDTO.setLogoImage(resource.getInputStream().readAllBytes());
		return companyDTO;

	}

}
