package com.api.gateway.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.api.gateway.model.Role;
import com.api.gateway.model.Token;
import com.api.gateway.model.User;
import com.api.gateway.repo.TokenRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtProvider {

	@Value("${jwt.secret}")
	public String secretKey;
	@Value("${jwt.tokenValidity}")
	public Long jwtTokenValidity;
	@Autowired
	ObjectMapper objectMapper;

	Logger log = LoggerFactory.getLogger(JwtProvider.class);

	@Autowired
	private TokenRepository tokenRepository;

	public Boolean isTokenExpired(String token) {

			Claims claims = getClaimFromToken(token);
			Optional<Token> authToken = tokenRepository.findByToken(token);
			if (authToken.isPresent()) {
				if (authToken.get().getExpired() || authToken.get().getExpiryDate().getTime() < new Date().getTime()) {
					throw new ExpiredJwtException(null, claims, "Session Expired,Please login again");
				}
			}
			return false;


	}

	public String generateToken(User user, HttpServletRequest httpServletRequest) {
		String authorities = new UsernamePasswordAuthenticationToken(user.getEmail(), null, user.getRoles().stream()
				.map(r -> new SimpleGrantedAuthority("ADMIN")).collect(Collectors.toList())).getAuthorities().stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));
		Map<String, Object> claims = new HashMap<>();
		claims.put("roles", user.getRoles());
		claims.put("userName", user.getEmail());
		claims.put("auth",authorities);
//		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
//				.commaSeparatedStringToAuthorityList("ROLE_ADMIN");
		claims.put("authorities",authorities);



		return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtTokenValidity * 1000))
				.signWith(SignatureAlgorithm.HS256, secretKey).compact();
	}

	public Date getExpirationDateFromToken(String token) {
		Date expiration;
		try {
			final Claims claims = getClaimFromToken(token);
			expiration = claims.getExpiration();
		} catch (Exception e) {
			expiration = null;
		}
		return expiration;
	}

	public Claims getClaimFromToken(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();

	}

	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}

	public String getUsernameFromToken(String token) {

		String username;
		try {
			final Claims claims = getClaimFromToken(token);
			username = (String) claims.get("userName");
		} catch (Exception e) {
			e.printStackTrace();
			username = null;
		}
		return username;

	}

	public List<Role> getRolesfromToken(String token) {
		try {
			final Claims claims = getClaimFromToken(token);
			return  objectMapper.convertValue(claims.get("roles"), new TypeReference<ArrayList<Role>>() {});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
}
