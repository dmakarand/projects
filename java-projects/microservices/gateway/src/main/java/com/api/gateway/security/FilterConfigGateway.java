package com.api.gateway.security;

import java.security.Key;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.server.ServerWebExchange;

import com.api.gateway.model.Account;
import com.api.gateway.multitenancy.TenantContext;
import com.api.gateway.repo.AccountRepository;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import reactor.core.publisher.Mono;

@Configuration
public class FilterConfigGateway implements GlobalFilter {
	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private UserDetailsService userDetailsService;

	final Logger logger = LoggerFactory.getLogger(FilterConfigGateway.class);
	private static List listOfOpenApis = Arrays.asList("/api/auth/user/login", "/api/company/getInfo",
			"/api/auth/user/getApiSecretKey");

	private List basicAuthAllowed = Arrays.asList("/api/auth/user/getApiSecretKey");

	@Value("${X-api-secret-key}")
	private String apiSecretKey;

	@Value("${X-api-secrey-value}")
	private String apiSecretValue;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		ServerHttpRequest servletRequest = exchange.getRequest();
		String auth = String.valueOf(servletRequest.getHeaders().get("Authorization"));
		auth = removeBraketsFromHeader(auth);
//
		if (!basicAuthAllowed.contains(servletRequest.getPath().toString())) {
			String apisecret = String.valueOf(servletRequest.getHeaders().get("X-api-secret"));
			apisecret = removeBraketsFromHeader(apisecret);
			try {
				validateSecreKet(apisecret);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!listOfOpenApis.contains(servletRequest.getPath().toString())) {

			if (String.valueOf(servletRequest.getHeaders().get("tenant")) != null) {
				String tenant = String.valueOf(servletRequest.getHeaders().get("tenant"));
				tenant = removeBraketsFromHeader(tenant);
				TenantContext.clear();
				Account account = accountRepository.findByName(tenant);
				try {
					if (account == null) {
						throw new Exception();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				tenant = tenant.concat("_auth");
				TenantContext.setCurrentTenant(tenant);
				try {
					if (auth != null) {

						String username = jwtProvider.getUsernameFromToken(auth);
						if (username != null) {

							UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

							if (jwtProvider.validateToken(auth, userDetails)) {
								UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
										userDetails, null, userDetails.getAuthorities());
								authentication.setDetails(new WebAuthenticationDetailsSource());
								logger.info("authenticated user " + username + ", setting security context");
								SecurityContextHolder.getContext().setAuthentication(authentication);
								logger.info("Global Pre Filter executed");
								logger.info("LOGGED IN USER AUTHORITIES" + SecurityContextHolder.getContext()
										.getAuthentication().getAuthorities().toString());
							}
						}

					}
				} catch (IllegalArgumentException e) {
					logger.error("An error occurred during getting username from token", e);
				} catch (ExpiredJwtException e) {
					logger.warn("The token is expired and not valid anymore", e);
				} catch (SignatureException e) {
					logger.error("Authentication Failed. Username or Password not valid.");
				} catch (Exception e) {
					logger.error("Authentication Failed Invaild Api Key error");
				}
			}

		}

		return chain.filter(exchange);

	}

	private void validateSecreKet(String apisecret) throws Exception {
		Key aesKey = new SecretKeySpec(apiSecretKey.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, aesKey);
		String decrypted = new String(cipher.doFinal(Base64.getDecoder().decode(apisecret)));
		if (!decrypted.equals(apiSecretValue)) {
			throw new Exception("Invalid api key");
		}
	}

	private String removeBraketsFromHeader(String header) {
		header = header.replaceAll("\\[", "").replaceAll("\\]", "");
		return header;
	}

}
