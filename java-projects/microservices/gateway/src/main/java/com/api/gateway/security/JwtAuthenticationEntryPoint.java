package com.api.gateway.security;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.api.gateway.model.ResponseDto;
import com.api.gateway.model.ResponseObject;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class represents exception handling for invalid jwt
 *
 * @author makarand
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private Logger log= LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);
    @Override public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e)
            throws IOException, ServletException {
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        log.info("Error from GATEWAY SERVICE");
        ResponseDto resp =
                new ResponseDto(new ResponseObject(HttpStatus.UNAUTHORIZED.value(),
                        "Unauthorzied Access to API", null),HttpStatus.OK);
        OutputStream outStream = httpServletResponse.getOutputStream();
        outStream.write(new ObjectMapper().writeValueAsString(resp).getBytes());
        outStream.flush();
    }
}
