package com.api.gateway.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class Snippet {
	@Bean
	SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http) {
	    return http
	        
	        .csrf().disable()
	        .build();
	}
}

