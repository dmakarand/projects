package com.api.gateway.model;

public enum TokenType {
    LOGIN_TOKEN,
    FORGOT_PASSWORD_TOKEN,
    RESET_PASSWORD_TOKEN
}
