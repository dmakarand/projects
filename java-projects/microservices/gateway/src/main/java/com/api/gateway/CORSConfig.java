package com.api.gateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class CORSConfig extends org.springframework.web.cors.CorsConfiguration {

	    @Bean
	    public CorsWebFilter corsWebFilter() {

	    final CORSConfig corsConfig = new CORSConfig();
	    corsConfig.setAllowedOrigins(Collections.singletonList("*"));
	    corsConfig.setMaxAge(3600L);
	    corsConfig.setAllowedMethods(Arrays.asList("GET", "POST"));
	    corsConfig.addAllowedHeader("*");

	    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", corsConfig);

	    return new CorsWebFilter(source);
	}

}
