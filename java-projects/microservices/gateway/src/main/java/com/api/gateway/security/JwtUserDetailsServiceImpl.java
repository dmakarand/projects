package com.api.gateway.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.api.gateway.model.User;
import com.api.gateway.repo.UserRepository;

/**
 * This class responsible for fetching user details from email
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> user = userRepo.findByEmail(email);

        if (user == null)
            throw new UsernameNotFoundException(String.format("No user exists with email : %s", email));
        else
            return JwtUserFactory.create(user.get());
    }



    
}
