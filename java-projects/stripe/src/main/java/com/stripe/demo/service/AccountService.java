package com.stripe.demo.service;

import com.stripe.Stripe;
import com.stripe.demo.dto.connectedAccounts.ConnectAccountDTO;
import com.stripe.exception.StripeException;
import com.stripe.model.Account;
import com.stripe.model.ExternalAccount;
import com.stripe.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.stripe.param.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

@Service
public class AccountService {

    @Autowired
    private HttpServletRequest servletRequest;

    @Value("${stripe.api.key}")
    private String apiKey;

    @PostConstruct
    public void setUp(){
        Stripe.apiKey = apiKey;
    }
    public void addConnectedAccounts(ConnectAccountDTO connectAccountDTO) throws StripeException {

        try {
            AccountCreateParams.Capabilities.CardPayments cardPayments = AccountCreateParams.Capabilities.CardPayments.builder()
                    .setRequested(true)
                    .build();

            AccountCreateParams.Capabilities.Transfers transfers = AccountCreateParams.Capabilities.Transfers.builder()
                    .setRequested(true)
                    .build();

            AccountCreateParams.Capabilities capabilities = AccountCreateParams.Capabilities.builder()
                    .setCardPayments(cardPayments)
                    .setTransfers(transfers)
                    .build();

            AccountCreateParams.BusinessProfile businessProfile = AccountCreateParams.BusinessProfile.builder()
                    .setName(connectAccountDTO.getAccountInfoDTO().getAccountName())
                    .setSupportEmail(connectAccountDTO.getAccountInfoDTO().getAccountEmail())
                    .build();

            AccountCreateParams.Individual.Address address = AccountCreateParams.Individual.Address.builder()
                    .setCity(connectAccountDTO.getAccountInfoDTO().getAccountCity())
                    .setCountry(connectAccountDTO.getAccountInfoDTO().getAccountCountry())
                    .setLine1(connectAccountDTO.getAccountInfoDTO().getAddressLine1())
                    .setPostalCode(connectAccountDTO.getAccountInfoDTO().getAccountZipCode())
                    .setState(connectAccountDTO.getAccountInfoDTO().getAccountState())
                    .build();

            AccountCreateParams.Individual.Dob dob = AccountCreateParams.Individual.Dob.builder()
                    .setDay(connectAccountDTO.getPersonalInfoDTO().getBirthDate())
                    .setMonth(connectAccountDTO.getPersonalInfoDTO().getBirthMonth())
                    .setYear(connectAccountDTO.getPersonalInfoDTO().getBirthYear())
                    .build();




            FileCreateParams fileCreateParamsFront = FileCreateParams.builder()
                    .setFile(connectAccountDTO.getFileFront().getInputStream())
                    .setPurpose(FileCreateParams.Purpose.IDENTITY_DOCUMENT)
                    .build();

            FileCreateParams fileCreateParamsBack = FileCreateParams.builder()
                    .setFile(connectAccountDTO.getFileBack().getInputStream())
                    .setPurpose(FileCreateParams.Purpose.IDENTITY_DOCUMENT)
                    .build();

            com.stripe.model.File fileFront = com.stripe.model.File.create(fileCreateParamsFront);

            com.stripe.model.File fileBack = com.stripe.model.File.create(fileCreateParamsBack);

            AccountCreateParams.Individual.Verification.Document document = AccountCreateParams.Individual.Verification.Document.builder()
                    .setBack(fileFront.getId())
                    .setFront(fileBack.getId())
                    .build();

            AccountCreateParams.Individual.Verification docsVerification = AccountCreateParams.Individual.Verification.builder()
                    .setDocument(document)
                    .build();

            AccountCreateParams.Individual individual = AccountCreateParams.Individual.builder()
                    .setAddress(address)
                    .setDob(dob)
                    .setEmail(connectAccountDTO.getAccountInfoDTO().getAccountEmail())
                    .setFirstName(connectAccountDTO.getPersonalInfoDTO().getFirstName())
                    .setLastName(connectAccountDTO.getPersonalInfoDTO().getLastName())
                    .setVerification(docsVerification)
                    .setIdNumber(connectAccountDTO.getIdNumber())
                    .build();
            AccountCreateParams.TosAcceptance tosAcceptance = AccountCreateParams.TosAcceptance.builder()
                    .setDate((long) System.currentTimeMillis() / 1000L)
                    .setIp(servletRequest.getRemoteAddr()) // Assumes you're not using a proxy
                    .build();


            AccountCreateParams newAccount = AccountCreateParams.builder()
                    .setCountry(connectAccountDTO.getAccountInfoDTO().getAccountCountry())
                    .setEmail(connectAccountDTO.getAccountInfoDTO().getAccountEmail())
                    .setBusinessType(AccountCreateParams.BusinessType.INDIVIDUAL)
                    .setType(AccountCreateParams.Type.CUSTOM)
                    .setDefaultCurrency(connectAccountDTO.getAccountInfoDTO().getCurrency())
                    .setIndividual(individual)
                    .setBusinessProfile(businessProfile)
                    .setTosAcceptance(tosAcceptance)
                    .setCapabilities(capabilities)
                    .build();

            Account account = Account.create(newAccount);

            TokenCreateParams.BankAccount bankAccount = TokenCreateParams.BankAccount.builder()
                    .setAccountHolderName(connectAccountDTO.getAccountInfoDTO().getAccountHolderName())
                    .setAccountHolderType(TokenCreateParams.BankAccount.AccountHolderType.INDIVIDUAL)
                    .setAccountNumber(connectAccountDTO.getAccountInfoDTO().getAccountNumber())
                    .setCountry(connectAccountDTO.getAccountInfoDTO().getAccountCountry())
                    .setCurrency(connectAccountDTO.getAccountInfoDTO().getCurrency())
                    .setRoutingNumber(connectAccountDTO.getAccountInfoDTO().getRoutingNumber())
                    .build();

            TokenCreateParams tokenCreateParams = TokenCreateParams.builder()
                    .setBankAccount(bankAccount)
                    .build();
            Token token = Token.create(tokenCreateParams);
            ExternalAccountCollectionCreateParams accountCollectionCreateParams = ExternalAccountCollectionCreateParams.builder()
                    .setExternalAccount(token.getId())
                    .build();

            ExternalAccount externalAccount = account.getExternalAccounts().create(accountCollectionCreateParams);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteAccount(String accountID)throws StripeException{
        Account account = Account.retrieve(accountID);
        account.delete();
    }
}
