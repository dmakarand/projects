package com.stripe.demo.dto.connectedAccounts;

public class PersonalInfoDTO {

    private String addressLine1;

    private Long birthDate;

    private Long birthMonth;

    private Long birthYear;

    private String firstName;

    private String lastName;

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public Long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Long birthDate) {
        this.birthDate = birthDate;
    }

    public Long getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(Long birthMonth) {
        this.birthMonth = birthMonth;
    }

    public Long getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Long birthYear) {
        this.birthYear = birthYear;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
