package com.stripe.demo.exception;

import com.stripe.demo.dto.ResponseDto;
import com.stripe.demo.dto.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<Object> serviceException(ServiceException serviceException){
        ResponseDto responseDto= new ResponseDto(new ResponseObject(HttpStatus.BAD_REQUEST.value(), serviceException.getMessage(),null),
                HttpStatus.OK);
        return new ResponseEntity<>(responseDto,HttpStatus.OK);
    }
}
