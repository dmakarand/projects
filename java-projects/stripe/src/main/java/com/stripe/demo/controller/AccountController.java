package com.stripe.demo.controller;

import com.stripe.demo.dto.connectedAccounts.ConnectAccountDTO;
import com.stripe.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/account")
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/connectAccount",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> addConnectedAccount(@ModelAttribute ConnectAccountDTO accountDTO) throws Exception {
        try {
            accountService.addConnectedAccounts(accountDTO);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/deleteAccount")
    public ResponseEntity<?> deleteAccount(@RequestParam("accountId") String accountId) throws Exception {
        try {
            accountService.deleteAccount(accountId);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}