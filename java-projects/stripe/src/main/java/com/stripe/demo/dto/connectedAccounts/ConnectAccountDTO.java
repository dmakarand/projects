package com.stripe.demo.dto.connectedAccounts;

import com.stripe.model.Account;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class ConnectAccountDTO implements Serializable {

    private AccountInfoDTO accountInfoDTO;

    private PersonalInfoDTO personalInfoDTO;

    private MultipartFile fileFront;

    private MultipartFile fileBack;

    private String idNumber;



    public AccountInfoDTO getAccountInfoDTO() {
        return accountInfoDTO;
    }

    public void setAccountInfoDTO(AccountInfoDTO accountInfoDTO) {
        this.accountInfoDTO = accountInfoDTO;
    }

    public PersonalInfoDTO getPersonalInfoDTO() {
        return personalInfoDTO;
    }

    public void setPersonalInfoDTO(PersonalInfoDTO personalInfoDTO) {
        this.personalInfoDTO = personalInfoDTO;
    }

    public MultipartFile getFileFront() {
        return fileFront;
    }

    public void setFileFront(MultipartFile fileFront) {
        this.fileFront = fileFront;
    }

    public MultipartFile getFileBack() {
        return fileBack;
    }

    public void setFileBack(MultipartFile fileBack) {
        this.fileBack = fileBack;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}
