package com.stripe.demo.dto.connectedAccounts;

import org.springframework.web.multipart.MultipartFile;

public class FileDTO {

    private MultipartFile fileFront;

    private MultipartFile fileBack;

    public MultipartFile getFileFront() {
        return fileFront;
    }

    public void setFileFront(MultipartFile fileFront) {
        this.fileFront = fileFront;
    }

    public MultipartFile getFileBack() {
        return fileBack;
    }

    public void setFileBack(MultipartFile fileBack) {
        this.fileBack = fileBack;
    }
}
