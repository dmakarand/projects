package com.stripe.demo.service;

import com.stripe.Stripe;
import com.stripe.demo.dto.*;
import com.stripe.demo.exception.ServiceException;
import com.stripe.demo.model.CustomerEntity;
import com.stripe.demo.repo.CustomerRepository;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import com.stripe.param.ChargeCreateParams;
import com.stripe.param.CustomerCreateParams;
import com.stripe.param.PaymentSourceCollectionCreateParams;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
public class CustomerService {

    @Value("${stripe.api.key}")
    private String apiKey;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CustomerRepository customerRepository;
    @PostConstruct
    public void setUp() {
        Stripe.apiKey = apiKey;
    }
    private static final Logger log = LoggerFactory.getLogger(CustomerService.class);
    @Transactional
    public void createCustomer(CustomerDTO customerDTO) throws StripeException {
        log.info("Creating customer :"+customerDTO.getEmail());
        String customerResponse = null;
        try {

            //save to local server
            CustomerEntity existingCustomer = customerRepository.findByEmail(customerDTO.getEmail());
            if(existingCustomer!=null){
               throw new ServiceException("User already exist with same email");
            }
            CustomerEntity customerEntity = modelMapper.map(customerDTO, CustomerEntity.class);
            customerEntity.setPassword(passwordEncoder.encode(customerDTO.getPassword()));
           CustomerEntity savedCustomer= customerRepository.save(customerEntity);
           if(savedCustomer.getId()==null){
               throw new ServiceException("Customer not saved");
           }
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new ServiceException(exception.getMessage());
        }

    }

    public CustomerCollection listCustomers() throws StripeException {
        Map<String, Object> params = new HashMap<>();
        params.put("limit", 10);
        CustomerCollection customers =
                Customer.list(params);

        return customers;
    }

    public Card addCard(CardDTO cardDTO) throws StripeException {
        Map<String, Object> params = new HashMap<>();
        params.put("number", cardDTO.getCardNumber());
        params.put("exp_month", cardDTO.getExpiryMonth());
        params.put("exp_year", cardDTO.getExpiryYear());
        params.put("cvc", cardDTO.getCvc());
        Map<String, Object> tokenParams = new HashMap<>();
        tokenParams.put("card", params);
        Token token = Token.create(tokenParams);

        Map<String, Object> sourceParams = new HashMap<>();
        sourceParams.put("source", token.getId());

        Customer customer = Customer.retrieve(cardDTO.getCustomerId());
        Card card =
                (Card) customer.getSources().create(sourceParams);
        return card;
    }

    public Charge charge(ChargeDTO chargeDTO) throws Exception {
        CustomerEntity customerEntity = customerRepository.findById(chargeDTO.getCustomerId()).orElseThrow(
                () -> new Exception("User is invalid"));

        CustomerDTO customerDTO = modelMapper.map(customerEntity,CustomerDTO.class);
        String customerId=saveCustomerToStripe(customerDTO,chargeDTO);
        Token cardToken = Token.retrieve(chargeDTO.getToken());
        PaymentSourceCollectionCreateParams paymentSourceCollectionCreateParams = PaymentSourceCollectionCreateParams.builder()
                .setSource(cardToken.getId())
                .build();
        Customer customer = Customer.retrieve(customerId);
        ChargeCreateParams chargeCreateParams = ChargeCreateParams.builder()
                .setAmount(chargeDTO.getAmount()).setCustomer(customerEntity.getStripeCustomerId())
                .setDestination(ChargeCreateParams.Destination.builder().setAccount(chargeDTO.getDestinationId()).build())
                .setCustomer(customer.getId())
                .setCurrency("INR")
                .build();

        Charge charge = Charge.create(chargeCreateParams);
        return charge;
    }

    public Customer retrieveCustomer(String customerId) throws StripeException {
        Customer customer = Customer.retrieve(customerId);
        return customer;
    }

    public Token createBankAccountToken(BankAccountDTO bankAccountDTO) throws Exception {
        Map<String, Object> bankAccount = new HashMap<>();
        bankAccount.put("country", bankAccountDTO.getCountry());
        bankAccount.put("currency", bankAccountDTO.getCurrency());
        bankAccount.put(
                "account_holder_name",
                bankAccountDTO.getAccountHolder()
        );
        bankAccount.put(
                "account_holder_type",
                "individual"
        );
        bankAccount.put("routing_number", bankAccountDTO.getRoutingNumber());
        bankAccount.put("account_number", bankAccountDTO.getAccountNumber());
        Map<String, Object> params = new HashMap<>();
        params.put("bank_account", bankAccount);

        Token token = Token.create(params);

        return token;
    }


    public Long getBalance(Long customerId) throws Exception {
        CustomerEntity customerEntity = customerRepository.findById(customerId).orElseThrow(
                () -> new Exception("User is invalid"));
        Customer customer = Customer.retrieve(customerEntity.getStripeCustomerId());
        if (customer == null) {
            throw new ServiceException("User is invalid");
        }
       return customer.getBalance();
    }

    public CustomerDTO login(LoginDTO loginDTO){
        if(loginDTO==null){
            throw new ServiceException("Bad Request");
        }
        CustomerEntity customerEntity = customerRepository.findByEmail(loginDTO.getUserName());
        if(customerEntity!=null){
            if(!passwordEncoder.matches(loginDTO.getPassword(),customerEntity.getPassword())){
                throw new ServiceException("Bad credentials");
            }
        }else{
            throw new ServiceException("User not  found");
        }
        CustomerDTO customerDTO = modelMapper.map(customerEntity,CustomerDTO.class);
        customerDTO.setId(customerEntity.getId());
        customerDTO.setPassword(null);
        return customerDTO;
    }


    private String saveCustomerToStripe(CustomerDTO customerDTO,ChargeDTO chargeDTO)throws StripeException{
        CustomerCreateParams.Address address = CustomerCreateParams.Address.builder()
                .setCity(customerDTO.getCity())
                .setState(customerDTO.getState())
                .setCountry(customerDTO.getCountry())
                .build();
        CustomerCreateParams customerCreateParams = CustomerCreateParams.builder()
                .setEmail(customerDTO.getEmail())
                .setName(customerDTO.getName())
                .setAddress(address)
                .setSource(chargeDTO.getToken())
                .build();
        Customer stripeCustomer = Customer.create(customerCreateParams);
        return stripeCustomer.getId();
    }
//    public ExternalAccountCollection getallBankAccounts()throws Exception{
//
//        Map<String, Object> retrieveParams =
//                new HashMap<>();
//        Customer customer =
//                Customer.retrieve(
//                        "cus_K7D2lWoscQkxx5");
//
//        Map<String, Object> params = new HashMap<>();
//        params.put("object", "bank_account");
//        params.put("limit", 3);
//            return    customer.getSources().list(params);
//    }
}
