package com.stripe.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChargeDTO {

    private Long customerId;

    private String token;

    private Long amount;

    private String destinationId;

    private String nameOnCard;

}
