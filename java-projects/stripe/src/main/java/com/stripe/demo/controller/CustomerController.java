package com.stripe.demo.controller;

import com.stripe.demo.dto.*;
import com.stripe.demo.exception.ServiceException;
import com.stripe.demo.service.CustomerService;
import com.stripe.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    private static final Logger log = LoggerFactory.getLogger(CustomerController.class);
    @PostMapping(value = "/create")
    public ResponseEntity<Customer> createCustomer(@RequestBody CustomerDTO customerDTO) throws Exception {
        log.info("Request arrived for register user");
        ResponseDto responseDto = null;
        try{
        customerService.createCustomer(customerDTO);
         responseDto =
                new ResponseDto(new ResponseObject(200, "User is registered successfully", null),
                        HttpStatus.OK);
        }
        catch (ServiceException exception){
            responseDto =
                    new ResponseDto(new ResponseObject(201, exception.getMessage(), null),
                            HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(responseDto, HttpStatus.OK);
    }

    @GetMapping(value = "/get")
    public CustomerCollection getAllCustomers() throws Exception {
        return customerService.listCustomers();
    }

    @PostMapping(value = "/create/card")
    public Card createCard(@RequestBody CardDTO cardDTO ) throws Exception {
       return customerService.addCard(cardDTO);
    }

    @PostMapping(value = "/charge")
    public ResponseEntity<Charge> charge(@RequestBody ChargeDTO chargeDTO) throws Exception {
       customerService.charge(chargeDTO);
        ResponseDto responseDto = new ResponseDto(new ResponseObject(200, "Payment successful", null),
                HttpStatus.OK);
        return new ResponseEntity(responseDto,HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/get/by/id")
    public Customer charge(@RequestParam("customerId") String customerId) throws Exception {
        return customerService.retrieveCustomer(customerId);
    }

    @PostMapping(value = "/bank/account/add")
    public Token charge(@RequestBody BankAccountDTO bankAccountDTO) throws Exception {
        return customerService.createBankAccountToken(bankAccountDTO);
    }

    @GetMapping(value = "/getBalance")
    public ResponseEntity<Long> getBalance(@RequestParam("customerId") Long customerId) throws Exception {
        return new ResponseEntity(customerService.getBalance(customerId),HttpStatus.ACCEPTED);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<CustomerDTO> login(@RequestBody LoginDTO loginDTO) throws Exception {
       CustomerDTO customerDTO=customerService.login(loginDTO);
        ResponseDto responseDto =
                new ResponseDto(new ResponseObject(200, "Login successful", customerDTO),
                        HttpStatus.OK);
        return new ResponseEntity(responseDto,HttpStatus.ACCEPTED);
    }


    @GetMapping(value = "/test")
    public String test(){
        return "hello";
    }
}
