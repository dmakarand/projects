package leetcode;

public class ShuffleArray {

    public void shuffle(int[] nums, int n) {
        int[] result = new int[nums.length];
        for (int i = 0; i < n; i++) {
            if (i % 2 != 0) {
                result[i] = nums[i - 1] + n;
            } else {
                result[i + 1] = nums[i];
            }
        }

        for (int i=0;i< result.length;i++){
            System.out.println(result[i]);
        }
    }

    public static void main(String[] args) {
        ShuffleArray shuffleArray = new ShuffleArray();
        shuffleArray.shuffle(new int[]{2,5,1,3,4,7},3);
    }
}

