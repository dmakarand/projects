package leetcode;

public class SumOf1DArray {

    public void sumOf1Darray(int[] nums) {
        int[] result = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {

                if (i == 0) {
                    result[i] = nums[i];
                } else {
                    result[i] = nums[i] + result[i - 1];
                }

        }
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }

    public static void main(String[] args) {
        SumOf1DArray sum = new SumOf1DArray();
        sum.sumOf1Darray(new int[]{1, 2});

    }
}
