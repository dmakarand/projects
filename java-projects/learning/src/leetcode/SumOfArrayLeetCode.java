package leetcode;

public class SumOfArrayLeetCode {
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        int i, j;
        for (i = 0; i < nums.length; i++) {
            for (j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    result[0] = i;
                    result[1] = j;
                    return result;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2, 7, 11, 15};
        int target = 9;
        SumOfArrayLeetCode sumOfArrayLeetCode = new SumOfArrayLeetCode();
        int[] sum = sumOfArrayLeetCode.twoSum(nums, target);
        for (int k = 0; k < sum.length; k++) {
            System.out.println(sum[k]);
        }
    }
}
