package leetcode;

public class FinalValueAfterOperations {
    public int finalValueAfterOperations(String[] operations) {
        int sum = 0;
        for (String s:operations){

            if(s.equalsIgnoreCase("++x") || s.equalsIgnoreCase("x++")){
                sum = sum+1;
            }else{
                sum = sum-1;
            }

        }
        System.out.println(sum);
        return 0;
    }

    public static void main(String[] args) {
        FinalValueAfterOperations finalValueAfterOperations = new FinalValueAfterOperations();
        finalValueAfterOperations.finalValueAfterOperations(new String[]{"--X","X++","X++"});
    }
}
