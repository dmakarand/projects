package pattern;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {
        List<List<String>> nestedList = Arrays.asList(
                Arrays.asList("makarand", "dipak"),
                Arrays.asList("akshay", "manoj")
        );


//        int result = 0;
//        for (Integer i : testList) {
//
//            result = i * i;
//        }
//
//        Map<Integer, Integer> resultMap = testList.stream().
//                filter(x -> x > 2).collect(Collectors.toMap(x -> x, x -> x.intValue()));
//
//        List<Employee> employees =
//                Arrays.asList(new Employee(1, "makarand"), new Employee(2, "akshay"),
//                        new Employee(3, "dipak"));
//
//        Map<Integer, Employee> collect = employees.stream().collect(Collectors.toMap(x -> x.getId(), x -> x));
//
//
        List<String> stringList = nestedList.stream().flatMap(x -> x.stream()).collect(Collectors.toList());
        System.out.println("Before :"+nestedList);


        Consumer<String> c= new Consumer<String>() {
            @Override
            public void accept(String string) {
                System.out.println(string);
            }
        };
        stringList.forEach(c);


        List<Integer> integers = Arrays.asList(1,2,3,4,4);
        Map<Integer, Integer> collect = integers.stream().collect(Collectors.toMap(x -> x,
                Function.identity(),
                (existing, replacement) -> existing));
        System.out.println(collect);
    }

}
