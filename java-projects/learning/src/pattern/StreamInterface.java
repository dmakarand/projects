package pattern;

public interface StreamInterface {

    public void print();
}
