package pattern;

public class Pattern {

    public void printPattern(int n){
        for (int i=n;i>0;i--){
            for (int j=n;j>=i;j++){
                System.out.print(j + "\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Pattern pattern = new Pattern();
        pattern.printPattern(5);
    }
}
