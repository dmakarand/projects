package pattern;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Stream {

    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList();
        numbers.add(12);
        numbers.add(1);
        numbers.add(3);

//        Integer result = numbers.stream().map(x -> x * x).reduce(0, Integer::sum);
//        System.out.println("Result "+result);
//        int sum=0;
//        for (Integer num:numbers){
//            sum=sum+(num*num);
//        }
//        System.out.println("Sum "+sum);
//
//        Set<Integer> collect = numbers.stream().map(x -> x + x).collect(Collectors.toSet());
//        System.out.println("Stream set "+collect);
//        System.out.println("Original array list"+numbers);
//        StreamInterface streamInterface=Stream::new;
//        streamInterface.print();

//        Consumer<Integer> c= new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) {
//                System.out.println(integer);
//            }
//        };
//        numbers.forEach(c);
//       numbers.stream()
//                .collect(Collectors.toMap(
//                        value
//                                -> value,
//                        value -> value.intValue()));
//        Map<Integer, Integer> integerIntegerMap = numbers.stream().collect(Collectors.toMap(x -> x, x -> x.intValue()));
//        System.out.println(integerIntegerMap);

        List<Employee> employees = Arrays.asList(new Employee(1, "mak"),
                new Employee(1, "mak"),
                new Employee(2, "mak1"));

        employees.stream().
                collect(Collectors.toMap(x -> x.getId(), Function.identity(),
                        (existing, replacement) -> existing));

//        employees.stream().flatMap(x->x.str)


        List<List<String>> strList = Arrays.asList(
                Arrays.asList("Makarand"),
                Arrays.asList("Akshay")
        );

        List<String> stringList = strList.stream().flatMap(x -> x.stream().map(y -> y.toLowerCase())).collect(Collectors.toList());
        System.out.println("Flat mapped string :" + stringList);


    }

    public <T extends Integer> T callFriend(String name, Class<T> type) {
        return null;
    }
}
