package pattern;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapDemo {

    public static void main(String[] args) {
        List<List<String>> strlist = Arrays.asList(
                Arrays.asList("MAKARAND"),
                Arrays.asList("AKSHAY"),
                Arrays.asList("AGSFT")
        );

        List<String> collect = strlist
                .stream()
                .flatMap(x -> x.stream().map(y -> y.toLowerCase())).collect(Collectors.toList());
        System.out.println("flatened :"+collect);
    }
}
