import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse  } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor( public http: HttpClient) { }



  loginUser(requestObj: any): Observable<any>{

    return this.http.post(environment.login, requestObj);
  }

  registerUser(requestObj: any): Observable<any>{

    return this.http.post(environment.register, requestObj);
  }

  charge(requestObj: any): Observable<any>{

    return this.http.post(environment.charge, requestObj);
  }
}
