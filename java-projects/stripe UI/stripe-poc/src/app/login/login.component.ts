import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpService } from '../Services/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isRegistered: boolean = false;
  show = false;
  buttonName = 'Show';
  loginModal: any = {}

  loginUserSubscription: Subscription;
  registerUserSubscription: Subscription

  constructor(private httpService: HttpService, private router: Router) { }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    if (this.loginUserSubscription) {
      this.loginUserSubscription.unsubscribe();
    }

    if (this.registerUserSubscription) {
      this.registerUserSubscription.unsubscribe();
    }
  }

  onSubmit() {
    console.log('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginModal))
  }

  registerHere() {
    this.isRegistered = !this.isRegistered;

    this.show = !this.show;

    if (this.show) {
      this.buttonName = 'Hide'
      console.log(this.show)
    }
    else {
      this.buttonName = 'Show'
    }
  }

  userLogin() {
    console.log("userLogin.", this.loginModal);

    this.loginUserSubscription = this.httpService.loginUser(this.loginModal).subscribe(res => {
      console.log("response login - ", res)
      const responseData = res.responseObject ? res.responseObject : {};
      if (responseData.statusCode == 200) {
        alert(responseData.message);        
        localStorage.setItem("id", responseData.object.id);
        localStorage.setItem("name", responseData.object.name);
        this.router.navigate(['checkout']);
      }else {
        alert(responseData.message);
      }
    }, error => {
      console.log("Error login - ", error)
      alert(error.statusText)
    }, () => {

    })
  }

  userRegister() {
    this.registerUserSubscription = this.httpService.registerUser(this.loginModal).subscribe(res => {
      const responseData = res.responseObject ? res.responseObject : {};
      if (res.responseObject.statusCode == 200) {
        alert(responseData.message);
        const email = this.loginModal.email;
        this.loginModal = {};
        this.loginModal.userName = email;
        this.isRegistered = false;
      } 
    }, error => {
      console.log("Error register - ", error);
    }, () => {

    })
  }

}
