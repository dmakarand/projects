import { Component, OnInit, ViewChild } from '@angular/core';

import { switchMap } from 'rxjs/operators';

import { StripeService, StripeCardComponent } from 'ngx-stripe';
import { StripeCardElementOptions, StripeElementsOptions } from '@stripe/stripe-js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../Services/http.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  // @ViewChild(StripeCardComponent) card: StripeCardComponent;

  public stripeTest: FormGroup;
  public chargeSubscription: Subscription;
  // public cardOptions: StripeCardElementOptions = {
  //   hidePostalCode: false,
  //   style: {
  //     base: {
  //       iconColor: '#666EE8',
  //       color: '#31325F',
  //       fontWeight: '300',
  //       fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
  //       fontSize: '14px',
  //       '::placeholder': {
  //         color: '#CFD7E0',
  //       },
  //     },
  //   },
  // };

  // elementsOptions: StripeElementsOptions = {
  //   locale: 'es',
  // };



  constructor(private httpService: HttpService, private router: Router) {


  }

  ngOnInit(): void {
    // this.stripeTest = this.fb.group({
    //   name: ['', [Validators.required]]
    // });

    // this.loadStripe();
  }
  ngOnDestroy() {
    if (this.chargeSubscription) {
      this.chargeSubscription.unsubscribe();
    }
  }


  // createToken(): void {
  //   const name = this.stripeTest.get('name')!.value;
  //   this.stripeService
  //     .createToken(this.card.element, { name })
  //     .subscribe((result) => {
  //       if (result.token) {
  //         // Use the token
  //         console.log(result.token.id);
  //       } else if (result.error) {
  //         // Error creating the token
  //         console.log(result.error.message);
  //       }
  //     });
  // }


  loadStripe() {
    if (!window.document.getElementById('stripe-script')) {
      var s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://checkout.stripe.com/checkout.js";
      window.document.body.appendChild(s);
    }
  }


  chargeCreditCard() {
    let form = document.getElementsByTagName("form")[0];
    (<any>window).Stripe.card.createToken({
      number: form.cardNumber.value,
      exp_month: form.expMonth.value,
      exp_year: form.expYear.value,
      cvc: form.cvc.value,
      name: localStorage.getItem("name")
    }, (status: number, response: any) => {
      if (status === 200) {
        const requestObj = {
          "customerId": localStorage.getItem("id"),
          "token": response.id,
          "amount": form.amount.value,
          "destinationId": form.athleteId.value,
          "nameOnCard": form.nameOnCard.value
        }
        this.chargeCard(requestObj);
      } else {
        console.log("response", response);
        console.log();
      alert(response.error.message)

      }
    });
  }

  chargeCard(token: any) {
    console.log("charge object-", token);
    this.chargeSubscription = this.httpService.charge(token).subscribe(res => {
      const responseData = res.responseObject ? res.responseObject : {};
      console.log("charge response-", res);

      if (res.responseObject.statusCode == 200) {
        alert(responseData.message);
      }
    }, error => {
      console.log("Error charge - ", error);
      alert(error.statusText)
    }, () => {

    })
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}


