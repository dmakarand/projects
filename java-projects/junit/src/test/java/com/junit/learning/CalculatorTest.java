package com.junit.learning;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

 class CalculatorServiceImpl implements  CalculatorService{

    @Override
    public int[] retrieveAllData() {
        return new int[] {1,2};
    }
}
public class CalculatorTest {

    @Test
    public void calculateSumTest() {
        Calculator calculatorTest = new Calculator();
        int actualResult = calculatorTest.calculateSum(5, 5);
        int expectedResult = 10;
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void calculateSumUsingService() {
        Calculator calculator = new Calculator();
        calculator.setCalculatorService(new CalculatorServiceImpl());
        int result = calculator.calculateSumUsingService();
        int expectedResult = 3;
        Assertions.assertEquals(expectedResult, result);
    }
}
