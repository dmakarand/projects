package com.junit.learning;

public class Calculator {

    CalculatorService calculatorService;

    public void setCalculatorService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    public int calculateSum(int a, int b){
        return a+b;
    }

    public int calculateSumUsingService(){
        int[] inputs = calculatorService.retrieveAllData();
        int sum=0;
       for(int a:inputs){
           sum= a+sum;
       }
       return sum;
    }


}
